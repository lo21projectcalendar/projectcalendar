#include "projectwindow.h"

ProjectWindow::ProjectWindow(Project &project, QWidget *parent) : QMainWindow(parent), _project(project)
{

    _taskInspectorWidget = 0;
    _projectInspectorWidget = 0;

    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    setUpGraphCentralWidget();
    setUpProjectDockWidget();
    setUpInspectorDockWidget();



}

void    ProjectWindow::setUpGraphCentralWidget()
{

    _centerScrollArea = new QScrollArea(this);
    _centerScrollArea->setAlignment(Qt::AlignCenter);

    setCentralWidget(_centerScrollArea);

    setUpGraphWidget();

}

void    ProjectWindow::setUpGraphWidget()
{
    _graphWidget = new TaskDependenciesGraphWidget(_project, this);
    _graphWidget->setRootTask(0);
    _graphWidget->setMinimumSize(500, 500);
    QObject::connect(_graphWidget, SIGNAL(taskSelected(Task*)), this, SLOT(slotTaskSelected(Task*)));


    _centerScrollArea->setWidget(_graphWidget);
}

void ProjectWindow::setUpProjectDockWidget()
{
    _projectDockWidget = new QDockWidget("Project", this);
    _projectDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    _projectDockWidget->setFloating(false);
    _projectDockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
    addDockWidget(Qt::LeftDockWidgetArea, _projectDockWidget);



    setUpProjectWidget();
}

void ProjectWindow::setUpProjectWidget()
{


    _projectTasksView = new ProjectTasksView(_project, _projectDockWidget);
    QObject::connect(_projectTasksView, SIGNAL(taskSelected(Task*)), this, SLOT(slotTaskSelected(Task*)));
    QObject::connect(_projectTasksView, SIGNAL(taskAdded(Task&)), this, SLOT(slotTaskAdded(Task&)));
    QObject::connect(_projectTasksView, SIGNAL(projectSelected()), this, SLOT(slotProjectSelected()));

    _projectDockWidget->setWidget(_projectTasksView);

}

void ProjectWindow::setUpInspectorDockWidget()
{
    _inspectorDockWidget = new QDockWidget("Inspector", this);
    _inspectorDockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
    _inspectorDockWidget->setFloating(false);
    _inspectorDockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
    addDockWidget(Qt::RightDockWidgetArea, _inspectorDockWidget);



    setUpProjectInspectorWidget();

}

void ProjectWindow::setUpTaskInspectorWidget(Task *task)
{



        if (_taskInspectorWidget)
        {
            QObject::disconnect(_taskInspectorWidget, SIGNAL(taskUpdated(Task&)), this, SLOT(slotTaskUpdated(Task&)));
            QObject::disconnect(_taskInspectorWidget, SIGNAL(taskDeleted(Task&)), this, SLOT(slotTaskDeleted(Task&)));
        }

        _taskInspectorWidget = new TaskInspectorWidget(task);
        QObject::connect(_taskInspectorWidget, SIGNAL(taskUpdated(Task&)), this, SLOT(slotTaskUpdated(Task&)));
        QObject::connect(_taskInspectorWidget, SIGNAL(taskDeleted(Task&)), this, SLOT(slotTaskDeleted(Task&)));
        _inspectorDockWidget->setWidget(_taskInspectorWidget);






}

void ProjectWindow::setUpProjectInspectorWidget()
{


        _projectInspectorWidget = new ProjectInspectorWidget(&_project);
        QObject::connect(_projectInspectorWidget, SIGNAL(projectUpdated(Project&)), this, SLOT(slotProjectUpdated(Project&)));
        QObject::connect(_projectInspectorWidget, SIGNAL(projectDeleted(Project&)), this, SLOT(slotProjectDeleted(Project&)));

        _inspectorDockWidget->setWidget(_projectInspectorWidget);



}

/****************************************************************/


void ProjectWindow::slotTaskDeleted(Task &task)

{

    qDebug() << "DELETE";
    TaskManager::getInstance().deleteTask(task);
    _graphWidget->updateGraph();
    _projectTasksView->deleteTask(task);
    _taskInspectorWidget->setTask(0);
    _inspectorDockWidget->setWidget(0);

}

void ProjectWindow::slotTaskSelected(Task* task)
{


       _graphWidget->setRootTask(task->getFather());
        setUpTaskInspectorWidget(task);


}


void ProjectWindow::slotTaskUpdated(Task& task)
{

            _graphWidget->setRootTask(task.getFather());
            _projectTasksView->updateWidget();
            setUpTaskInspectorWidget(&task);

}



void ProjectWindow::slotTaskAdded(Task& task)
 {
    _graphWidget->updateGraph();
    _projectTasksView->updateWidget();

    slotTaskSelected(&task);

}

void ProjectWindow::slotProjectSelected()
{
    setUpProjectInspectorWidget();
}

void ProjectWindow::slotProjectUpdated(Project &)
{
    _projectTasksView->updateWidget();
}

void ProjectWindow::slotProjectDeleted(Project &project)
{
    ProjectManager::getInstance().deleteProject(project);
    this->close();
}


void ProjectWindow::closeEvent(QCloseEvent*)
{
    emit closed();
}
