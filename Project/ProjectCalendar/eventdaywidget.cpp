#include "eventdaywidget.h"


/*-----------------*/
/*EVENT WIDGET*/

EventWidget::EventWidget(const Event &event, QWidget *parent) : QWidget(parent), _event(event)
{



    QPalette p(palette());
    p.setColor(QPalette::Background, QColor(200,200,200));
    setAutoFillBackground(true);
    setPalette(p);

   QGridLayout *layout = new QGridLayout;
   layout->setMargin(0);
   layout->setSpacing(0);

   QString title = "<center><b>";
   title.append(_event.getName());
   title.append("</b>");

   if (const EventTask* et = dynamic_cast<const EventTask*>(&_event))
   {
       const UnitTask& ut = et->getTask();

       title.append("<br /><i>");
       title.append(ut.getProject().getName());
       title.append(" - ");
       title.append(ut.getTitle());
       title.append("</i>");

   }

   title.append("</center>");

   QLabel *titleLabel = new QLabel(title);
   QFont f( "Arial", 10, QFont::Normal);
     titleLabel->setFont( f);
   titleLabel->setWordWrap(true);
   layout->addWidget(titleLabel, 0, 0, 1, 1, Qt::AlignCenter);



   setLayout(layout);

}

void EventWidget::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);

    painter.drawRect(0,0,width()-1, height()-1);

    QWidget::paintEvent(event);
}

/*-----------------*/
/*EVENT DAY WIDGET*/

EventDayWidget::EventDayWidget(const QDate& date, unsigned int scale,  QWidget *parent) : QWidget(parent), _date(date), _scale(scale), _gridLayout(0)
{


    this->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
    this->setMinimumHeight(1500);
    this->setFixedWidth(100);
    updateDay();



}

void EventDayWidget::updateDay()
{

    this->setMaximumHeight(60*24/_scale*50);



    for (EventManager::Iterator it = EventManager::getInstance().iterator(); !it.isDone(); it.next())
    {
        Event& e = it.currentItem();

        if (e.getDateDebut().date() <= _date && e.getDateFin().date() >= _date)
        {


            QTime startTime = (e.getDateDebut().date() < _date) ? QTime(0,0) : e.getDateDebut().time();
            QTime endTime  = (e.getDateFin().date() > _date) ? QTime(23,59) : e.getDateFin().time();

            unsigned int startNbMinutes = startTime.hour()*60+startTime.minute();
            unsigned int endNbMinutes = endTime.hour()*60+endTime.minute();
            unsigned int deltaNbMinutes = endNbMinutes - startNbMinutes;

            double row = (double)startNbMinutes / _scale;
            double rowSpan = (double)deltaNbMinutes / _scale;

            QString detail = e.getName();

            EventWidget *eventWidget = new EventWidget(e, this);

            eventWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
            eventWidget->setGeometry(0+4, row*50+10+4, 100-8, rowSpan*50-8);



        }



    }


    int x1, x2, y1, y2;


    for (int i = 0; i < 24; ++i)
    {
        x1 = 0;
        x2 = 100;
        y1 = y2 = i*50+10;



        QWidget *line = new QWidget(this);
        line->setGeometry(x1, y1, x2-x1, 1);
        QPalette p(palette());
        p.setColor(QPalette::Background,QColor(150,150,150));
        line->setAutoFillBackground(true);
        line->setPalette(p);
    }



}



