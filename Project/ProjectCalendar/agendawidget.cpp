#include "agendawidget.h"

AgendaWidget::AgendaWidget(QDate week, unsigned int scale, QWidget *parent) :  QWidget(parent), _week(week),  _gridLayout(0), _scale(scale)
{

    setWeekDate(week);
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    this->setMinimumSize(1000, 1500);

}

void AgendaWidget::updateAgenda()
{

    if (_gridLayout)
    {


        QLayoutItem * item;
        QLayout * sublayout;
        QWidget * widget;
        while ((item = layout()->takeAt(0))) {
            if ((sublayout = item->layout()) != 0) {/* do the same for sublayout*/}
            else if ((widget = item->widget()) != 0) {widget->hide(); delete widget;}
            else {delete item;}
        }

        delete _gridLayout;

    }

    _gridLayout = new QGridLayout();


    QLabel *dateLabel = new QLabel("<center><i>Hour</i></center>");
    _gridLayout->addWidget(dateLabel, 0, 0);

    AgendaTimelineWidget *timeline = new AgendaTimelineWidget(_scale);
    _gridLayout->addWidget(timeline, 1, 0);

    for (int i = 0; i < 7; ++i)
    {
        QString string = "<center><b>";
        string.append(_week.addDays(i).toString());
        string.append("</b></center>");
        QLabel *dateLabel = new QLabel(string);

        _gridLayout->addWidget(dateLabel, 0, i+1);

        EventDayWidget *dw = new EventDayWidget(_week.addDays(i), _scale);
        _gridLayout->addWidget(dw, 1, i+1);

    }

    setLayout(_gridLayout);
}


void AgendaWidget::setWeekDate(QDate week)
{
    _week = week;
    while (_week.dayOfWeek() != 1)
        _week = _week.addDays(-1);

    updateAgenda();
}
