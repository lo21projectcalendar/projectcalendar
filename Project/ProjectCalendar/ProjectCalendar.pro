#-------------------------------------------------
#
# Project created by QtCreator 2015-04-30T17:48:54
#
#-------------------------------------------------

QT       += core gui
QT       += xml

CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjectCalendar
TEMPLATE = app


SOURCES += main.cpp\
    calendar.cpp \
    timing.cpp \
    projectwindow.cpp \
    taskdependenciesgraphwidget.cpp \
    projecttasksview.cpp \
    taskinspectorwidget.cpp \
    projectinspectorwidget.cpp \
    eventwindow.cpp \
    eventdaywidget.cpp \
    agendawidget.cpp \
    agendatimelinewidget.cpp \
    projecteventswidget.cpp

HEADERS  += \
    calendar.h \
    timing.h \
    projectwindow.h \
    taskdependenciesgraphwidget.h \
    projecttasksview.h \
    taskinspectorwidget.h \
    projectinspectorwidget.h \
    eventwindow.h \
    eventdaywidget.h \
    agendawidget.h \
    agendatimelinewidget.h \
    projecteventswidget.h

FORMS    +=

RESOURCES += \
    projectcalendar.qrc
