#include "projectinspectorwidget.h"

ProjectInspectorWidget::ProjectInspectorWidget(Project *project, QWidget *parent) : QWidget(parent), _project(project)
{
    setup();
}

void ProjectInspectorWidget::setup()
{
    if (!_project)
        return;

    QVBoxLayout *vLayout = new QVBoxLayout();

    QFormLayout *fLayout = new QFormLayout();

    _titleLineEdit = new QLineEdit(_project->getName());
    connect(_titleLineEdit, SIGNAL(returnPressed()), this, SLOT(titleUpdated()));


    fLayout->addRow("Name : ", _titleLineEdit);

    QDateEdit *availableDateEdit = new QDateEdit(_project->getAvailable());
availableDateEdit->setEnabled(false);
    fLayout->addRow("Available : ", availableDateEdit);

    QDateEdit *deadlineDateEdit = new QDateEdit(_project->getDeadline());
    deadlineDateEdit->setEnabled(false);

    fLayout->addRow("Deadline : ", deadlineDateEdit);

    vLayout->addLayout(fLayout);

    _deleteButton = new QPushButton("Delete");

    QObject::connect(_deleteButton, SIGNAL(clicked(bool)), this, SLOT(deleteProject(bool)));

    vLayout->addWidget(_deleteButton);

    setLayout(vLayout);
}

void ProjectInspectorWidget::titleUpdated()
{

    try
    {
        _project->setName(_titleLineEdit->text());

    }
    catch(CalendarException e)
    {
        _titleLineEdit->setText(_project->getName());
          QMessageBox::information(this, "Error", e.getInfo());
    }


    emit projectUpdated(*_project);
}
void ProjectInspectorWidget::deleteProject(bool)
{
    emit projectDeleted(*_project);
}
