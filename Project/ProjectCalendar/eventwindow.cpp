#include "eventwindow.h"
#include "calendar.h"
#include<typeinfo>

EventWindow::EventWindow(){

    _actionAddEventTrad = new QAction("Traditional Event", this);
    _actionAddEventTask = new QAction("Task Event",  this);
    _actionAddProject = new QAction("Project",  this);
    _actionLoadHD = new QAction("From Hard Drive",  this);

    _actionSaveXML = new QAction("XML",  this);

    _actionQuitter = new QAction("Quitter",this);

    connect(_actionAddEventTrad, SIGNAL(triggered()), this, SLOT(slotInterfaceAddEvtTrad()));
    connect(_actionAddEventTask, SIGNAL(triggered()), this, SLOT(slotInterfaceAddEvtTask()));
    connect(_actionAddProject, SIGNAL(triggered()), this, SLOT(slotAddProject()));
    connect(_actionLoadHD, SIGNAL(triggered()), this, SLOT(slotOpenHardDrive()));
    connect(_actionSaveXML, SIGNAL(triggered()), this, SLOT(slotSaveXML()));

    connect(_actionQuitter, SIGNAL(triggered()), this, SLOT(close()));


    _menuFichier = menuBar()->addMenu("&File");



    _menuNew = _menuFichier->addMenu("&New");
    _menuNew->addAction(_actionAddProject);
    _menuNew->addSeparator();
    _menuNew->addAction(_actionAddEventTrad);
    _menuNew->addAction(_actionAddEventTask);

    _menuOpen = _menuFichier->addMenu("&Open");
    _menuOpen->addAction(_actionLoadHD);


    _menuSave = _menuFichier->addMenu("&Save");
    _menuSave->addAction(_actionSaveXML);


    _menuFichier->addSeparator();
    _menuFichier->addAction(_actionQuitter);

    _agenda = new AgendaWidget(QDate::currentDate(),60);


    QScrollArea *scroll = new QScrollArea();
    scroll->setWidget(_agenda);
    scroll->setWidgetResizable(true);
    setCentralWidget(scroll);


    _dockRight = new QDockWidget("Inspector", this);
    _dockRight->setFloating(false);
    _dockRight->setFeatures(QDockWidget::NoDockWidgetFeatures);

    this->addDockWidget(Qt::RightDockWidgetArea, _dockRight);

    _dockLeft= new QDockWidget("Calendar", this);
    _dockLeft->setFloating(false);
    _dockLeft->setFeatures(QDockWidget::NoDockWidgetFeatures);

    _eventsWidget = new ProjectEventsWidget;

    QObject::connect(_eventsWidget, SIGNAL(dateSelected(QDate)), this, SLOT(slotDateSelected(QDate)));
    QObject::connect(_eventsWidget, SIGNAL(projectSelected(Project&)), this, SLOT(slotInterfaceModifProject(Project&)));
    QObject::connect(_eventsWidget, SIGNAL(projectDoubleClicked(Project&)), this, SLOT(slotProjectDoubleClicked(Project&)));
    QObject::connect(_eventsWidget, SIGNAL(taskSelected(Task&)), this, SLOT(slotInterfaceModifTask(Task&)));

    QObject::connect(_eventsWidget, SIGNAL(selectEvtToModif(Event&)),this,SLOT(slotEventSelected(Event&)));
    QObject::connect(this, SIGNAL(signalChEvtTrad(const EventTraditionnal&)), this, SLOT(slotInterfaceModifEvtTrad(const EventTraditionnal&)));
    QObject::connect(this, SIGNAL(signalChEvtTask(const EventTask&)), this, SLOT(slotInspectorEvtTask(const EventTask&)));
    _dockLeft->setWidget(_eventsWidget);
    this->addDockWidget(Qt::LeftDockWidgetArea, _dockLeft);
}


void EventWindow::slotInterfaceAddEvtTrad(){
    _dockAddEvent = new QWidget;

    _labelNomEvt= new QLabel("Nom Event:");
    _NomEvt=new QLineEdit;

    _labelDescriptionEvt=new QLabel("Event description:");
    _DescriptionEvt=new QTextEdit;

    _labelNbPersoEvt=new QLabel("Nombre de personnes:");
    _nbPersoEvt=new QSpinBox;

    _labelDateEvt=new QLabel("Date de l'event:");
    _dateEvt=new QDateEdit;
    _dateEvt->setDate(QDate::currentDate());
    _timeEvt=new QTimeEdit;

    _labelDatefEvt=new QLabel("Date de fin de l'event:");
    _datefEvt=new QDateEdit;
    _datefEvt->setDate(QDate::currentDate());
    _timefEvt=new QTimeEdit;

    _saveEvent=new QPushButton("Save",_dockAddEvent);
    _cancelEvent=new QPushButton("Cancel",_dockAddEvent);

    coucheH1=new QHBoxLayout();
    coucheH1->addWidget(_labelNomEvt);
    coucheH1->addWidget(_NomEvt);

    coucheV1=new QVBoxLayout();
    coucheV1->addWidget(_labelDescriptionEvt);
    coucheV1->addWidget(_DescriptionEvt);

    coucheH2=new QHBoxLayout();
    coucheH2->addWidget(_labelNbPersoEvt);
    coucheH2->addWidget(_nbPersoEvt);

    coucheH3=new QHBoxLayout();
    coucheH3->addWidget(_labelDateEvt);
    coucheH3->addWidget(_dateEvt);
    coucheH3->addWidget(_timeEvt);

    coucheH4=new QHBoxLayout();
    coucheH4->addWidget(_labelDatefEvt);
    coucheH4->addWidget(_datefEvt);
    coucheH4->addWidget(_timefEvt);

    coucheH5=new QHBoxLayout();
    coucheH5->addWidget(_saveEvent);
    coucheH5->addWidget(_cancelEvent);

    QVBoxLayout *dockLayoutAE = new QVBoxLayout;
    dockLayoutAE->addLayout(coucheH1);
    dockLayoutAE->addLayout(coucheV1);
    dockLayoutAE->addLayout(coucheH2);
    dockLayoutAE->addLayout(coucheH3);
    dockLayoutAE->addLayout(coucheH4);
    dockLayoutAE->addLayout(coucheH5);

    _dockAddEvent->setLayout(dockLayoutAE);
    _dockAddEvent->show();

    QObject::connect(_saveEvent,SIGNAL(clicked()),this,SLOT(slotAddEvtTrad()));
    QObject::connect(_cancelEvent,SIGNAL(clicked()),_dockAddEvent,SLOT(close()));
}

void EventWindow::slotAddEvtTrad(){
    QDateTimeEdit* date=new QDateTimeEdit();
    QDateTimeEdit* datef=new QDateTimeEdit();

    date->setDate(_dateEvt->date());
    date->setTime(_timeEvt->time());
    datef->setDate(_datefEvt->date());
    datef->setTime(_timefEvt->time());

    if(EventManager::getInstance().isValid(date->dateTime(),datef->dateTime(),_nbPersoEvt->value())){
        EventManager::getInstance().addEventTrad(_NomEvt->text(),date->dateTime(),datef->dateTime(),_nbPersoEvt->value(),_DescriptionEvt->toPlainText());
        _dockAddEvent->close();


    }

    _eventsWidget->updateTreeView();
    _agenda->updateAgenda();

}

void EventWindow::slotInterfaceAddEvtTask(){
    _dockAddEvent = new QWidget;

    /** Parcour Liste des taches **/
    _listTask= new QComboBox();
    _listProject= new QComboBox();

    ProjectManager& pm=ProjectManager::getInstance();
    for (ProjectManager::Iterator it = pm.iterator(); !it.isDone(); it.next())
    {
        Project *p = dynamic_cast<Project*>(&it.currentItem());
        if (p)
        {
            _listProject->addItem(p->getName());
        }
    }

    Project& cp= ProjectManager::getInstance().getProject(_listProject->currentText());
    for(TaskManager::Iterator it = TaskManager::getInstance().getConstSchedulableIteratorForTask(cp);!it.isDone(); it.next())
    {
        UnitTask* ut = dynamic_cast<UnitTask*>(&it.currentItem());
        if (ut)
        {
            _listTask->addItem(ut->getTitle(),ut->getId());
        }
    }

    /** Fin Parcour Liste des Taches **/

    _labelNomEvt= new QLabel("Nom Event:");
    _NomEvt=new QLineEdit;

    _labelProjectTask = new QLabel("Projet associé à l'Event:");
    _labelNomTacheEvt = new QLabel("Tache associé à l'Event:");

    _labelDateEvt=new QLabel("Date debut de l'event:");
    _dateEvt=new QDateEdit;
    _dateEvt->setDate(QDate::currentDate());
    _timefEvt=new QTimeEdit;

    _labelDatefEvt=new QLabel("Date de fin de l'event:");
    _datefEvt=new QDateEdit;
    _datefEvt->setDate(QDate::currentDate());
    _timeEvt=new QTimeEdit;

    _saveEvent=new QPushButton("Save",_dockAddEvent);
    _cancelEvent=new QPushButton("Cancel",_dockAddEvent);

    coucheH1=new QHBoxLayout();
    coucheH1->addWidget(_labelNomEvt);
    coucheH1->addWidget(_NomEvt);

    coucheH2=new QHBoxLayout();
    coucheH2->addWidget(_labelProjectTask);
    coucheH2->addWidget(_listProject);

    coucheH3=new QHBoxLayout();
    coucheH3->addWidget(_labelNomTacheEvt);
    coucheH3->addWidget(_listTask);

    coucheH4=new QHBoxLayout();
    coucheH4->addWidget(_labelDateEvt);
    coucheH4->addWidget(_dateEvt);
    coucheH4->addWidget(_timeEvt);

    coucheH5=new QHBoxLayout();
    coucheH5->addWidget(_labelDatefEvt);
    coucheH5->addWidget(_datefEvt);
    coucheH5->addWidget(_timefEvt);

    coucheH6=new QHBoxLayout();
    coucheH6->addWidget(_saveEvent);
    coucheH6->addWidget(_cancelEvent);

    _dockLayoutAE = new QVBoxLayout;
    _dockLayoutAE->addLayout(coucheH1);
    _dockLayoutAE->addLayout(coucheH2);
    _dockLayoutAE->addLayout(coucheH3);
    _dockLayoutAE->addLayout(coucheH4);
    _dockLayoutAE->addLayout(coucheH5);
    _dockLayoutAE->addLayout(coucheH6);

    _dockAddEvent->setLayout(_dockLayoutAE);
    _dockAddEvent->show();

    //qDebug()<<p->getName();
    QObject::connect(_listProject,SIGNAL(currentIndexChanged(const QString &)),this,SLOT(slotChangeSelectedProject(const QString &)));
    QObject::connect(_saveEvent,SIGNAL(clicked()),this,SLOT(slotAddEvtTask()));
    QObject::connect(_cancelEvent,SIGNAL(clicked()),_dockAddEvent,SLOT(close()));
}

void EventWindow::slotAddEvtTask(){
    QDateTimeEdit* date=new QDateTimeEdit();
    QDateTimeEdit* datef=new QDateTimeEdit();

    date->setDate(_dateEvt->date());
    date->setTime(_timeEvt->time());
    datef->setDate(_datefEvt->date());
    datef->setTime(_timefEvt->time());

    qDebug()<<_listProject->currentText();

    Task& taskE = TaskManager::getInstance().getTask(_listTask->itemData(_listTask->currentIndex()).toString(),ProjectManager::getInstance().getProject(_listProject->currentText()));
    UnitTask *uTaskE = dynamic_cast<UnitTask*>(&taskE);

    if(EventManager::getInstance().isValid(date->dateTime(),datef->dateTime(),uTaskE)){
        EventManager::getInstance().addEventTask(_NomEvt->text(),date->dateTime(),datef->dateTime(),*uTaskE);
        _dockAddEvent->close();
    }

    _eventsWidget->updateTreeView();
    _agenda->updateAgenda();
}



void EventWindow::slotInterfaceModifEvtTrad(const EventTraditionnal& e){
    _dockAddEvent= new QWidget;
    _dockRight->setWidget(_dockAddEvent);
    _dockRight->setWindowTitle("Inspector");

    /** sav val event **/
    _oldDateDebut=new QDateTime(e.getDateDebut());
    _oldDateFin=new QDateTime(e.getDateFin());

    _labelNomEvt= new QLabel("Nom Event:");
    _NomEvt=new QLineEdit(e.getName());

    _labelDescriptionEvt=new QLabel("Event description:");
    _DescriptionEvt=new QTextEdit();
    _DescriptionEvt->setText(e.getDescription());

    _labelNbPersoEvt=new QLabel("Nombre de personnes:");
    _nbPersoEvt=new QSpinBox;

    _labelDateEvt=new QLabel("Date de l'event:");
    _dateEvt=new QDateEdit;
    _dateEvt->setDate(e.getDateDebut().date());
    _timeEvt=new QTimeEdit;
    _timeEvt->setTime(e.getDateDebut().time());

    _labelDatefEvt=new QLabel("Date de fin de l'event:");
    _datefEvt=new QDateEdit;
    _datefEvt->setDate(e.getDateFin().date());
    _timefEvt=new QTimeEdit;
    _timefEvt->setTime(e.getDateFin().time());

    _saveEvent=new QPushButton("Save",_dockAddEvent);
    _cancelEvent=new QPushButton("Cancel",_dockAddEvent);

    coucheH1=new QHBoxLayout();
    coucheH1->addWidget(_labelNomEvt);
    coucheH1->addWidget(_NomEvt);

    coucheV1=new QVBoxLayout();
    coucheV1->addWidget(_labelDescriptionEvt);
    coucheV1->addWidget(_DescriptionEvt);

    coucheH2=new QHBoxLayout();
    coucheH2->addWidget(_labelNbPersoEvt);
    coucheH2->addWidget(_nbPersoEvt);

    coucheH3=new QHBoxLayout();
    coucheH3->addWidget(_labelDateEvt);
    coucheH3->addWidget(_dateEvt);
    coucheH3->addWidget(_timeEvt);

    coucheH4=new QHBoxLayout();
    coucheH4->addWidget(_labelDatefEvt);
    coucheH4->addWidget(_datefEvt);
    coucheH4->addWidget(_timefEvt);

    coucheH5=new QHBoxLayout();
    coucheH5->addWidget(_saveEvent);
    coucheH5->addWidget(_cancelEvent);

    QVBoxLayout *dockLayoutAE = new QVBoxLayout;
    dockLayoutAE->addLayout(coucheH1);
    dockLayoutAE->addLayout(coucheV1);
    dockLayoutAE->addLayout(coucheH2);
    dockLayoutAE->addLayout(coucheH3);
    dockLayoutAE->addLayout(coucheH4);
    //dockLayoutAE->addLayout(coucheH5);

    _dockAddEvent->setLayout(dockLayoutAE);

    QObject::connect(_saveEvent,SIGNAL(clicked()),this,SLOT(slotModifEvtTrad()));
    QObject::connect(_cancelEvent,SIGNAL(clicked()),_dockAddEvent,SLOT(close()));

    _dockAddEvent->setEnabled(false);
}

void EventWindow::slotModifEvtTrad(){
    QDateTimeEdit* date=new QDateTimeEdit();
    QDateTimeEdit* datef=new QDateTimeEdit();

    qDebug()<<_oldDateFin->date();
    qDebug()<<_oldDateDebut->date();

    date->setDate(_dateEvt->date());
    date->setTime(_timeEvt->time());
    datef->setDate(_datefEvt->date());
    datef->setTime(_timefEvt->time());

    if(EventManager::getInstance().isModifValid(date->dateTime(),datef->dateTime(),_nbPersoEvt->value(),*_oldDateDebut,*_oldDateFin)){

       EventManager::getInstance().addEventTrad(_NomEvt->text(),date->dateTime(),datef->dateTime(),_nbPersoEvt->value(),_DescriptionEvt->toPlainText());
        _dockRight->close();
    }

    _eventsWidget->updateTreeView();
    _agenda->updateAgenda();
}

void EventWindow::slotInspectorEvtTask(const EventTask& e){

    _dockAddEvent = new QWidget;
    _dockRight->setWidget(_dockAddEvent);

    _labelNomEvt= new QLabel("Nom Event:");
    _NomEvt=new QLineEdit(e.getName());

    _labelProjectTask = new QLabel("Projet associé à l'Event:");
    _labelNomTacheEvt = new QLabel("Tache associé à l'Event:");

    _labelDateEvt=new QLabel("Date de l'event:");
    _dateEvt=new QDateEdit;
    _dateEvt->setDate(e.getDateDebut().date());
    _timeEvt=new QTimeEdit;
    _timeEvt->setTime(e.getDateDebut().time());

    _labelDatefEvt=new QLabel("Date de fin de l'event:");
    _datefEvt=new QDateEdit;
    _datefEvt->setDate(e.getDateFin().date());
    _timefEvt=new QTimeEdit;
    _timefEvt->setTime(e.getDateFin().time());

    coucheH1=new QHBoxLayout();
    coucheH1->addWidget(_labelNomEvt);
    coucheH1->addWidget(_NomEvt);

    coucheV1=new QVBoxLayout();
    coucheV1->addWidget(_labelProjectTask);
    coucheV1->addWidget(_listProject);

    coucheV2=new QVBoxLayout();
    coucheV2->addWidget(_labelNomTacheEvt);
    coucheV2->addWidget(_listTask);

    coucheH2=new QHBoxLayout();
    coucheH2->addWidget(_labelDateEvt);
    coucheH2->addWidget(_dateEvt);
    coucheH2->addWidget(_timeEvt);

    coucheH3=new QHBoxLayout();
    coucheH3->addWidget(_labelDatefEvt);
    coucheH3->addWidget(_datefEvt);
    coucheH3->addWidget(_timefEvt);

    _dockLayoutAE = new QVBoxLayout;
    _dockLayoutAE->addLayout(coucheH1);
    _dockLayoutAE->addLayout(coucheV1);
    _dockLayoutAE->addLayout(coucheV2);
    _dockLayoutAE->addLayout(coucheH2);
    _dockLayoutAE->addLayout(coucheH3);

    _dockAddEvent->setLayout(_dockLayoutAE);
    _dockAddEvent->setEnabled(false);
}

void EventWindow::slotInterfaceModifTask(Task& task)
{
    TaskInspectorWidget *inspector = new TaskInspectorWidget(&task);
    inspector->setEnabled(false);
    _dockRight->setWidget(inspector);
}


void EventWindow::slotInterfaceModifProject(Project& project)
{
    ProjectInspectorWidget *inspector = new ProjectInspectorWidget(&project);
    inspector->setEnabled(false);
    _dockRight->setWidget(inspector);
}

void EventWindow::slotChangeSelectedProject(const QString& nameProject){
    Project& cp= ProjectManager::getInstance().getProject(nameProject);
    _listTask->clear();

    for(TaskManager::Iterator it = TaskManager::getInstance().getSchedulableIteratorForTask(cp);!it.isDone(); it.next())
    {
        UnitTask* ut = dynamic_cast<UnitTask*>(&it.currentItem());
        if (ut)
        {
            _listTask->addItem(ut->getTitle(),ut->getId());
        }
    }
}

void EventWindow::slotAddProject()
{
    int i = 1;
    QString projectName = "";
    do
    {
        projectName = "New Project ";
        projectName.append(QString::number(i));
        ++i;
    } while ( ProjectManager::getInstance().containsProject(projectName) );

    Project& pro = ProjectManager::getInstance().addProject(projectName);
    slotProjectDoubleClicked(pro);
}

void EventWindow::slotOpenHardDrive()
{
    DataManager::loadData<DataManager::DataGetDriveFileStrategy, DataManager::DataLoadXMLStrategy>();
    _eventsWidget->updateTreeView();
    _agenda->updateAgenda();
}

void EventWindow::slotSaveXML()
{
    DataManager::saveData<DataManager::DataSaveXMLStrategy, DataManager::DataPostDriveFileStrategy>();
}



void EventWindow::slotDateSelected(QDate date)
{
    _agenda->setWeekDate(date);
}

void EventWindow::slotProjectDoubleClicked(Project& project)
{
    qDebug() << "Project : " << project.getName();
    ProjectWindow *pw = new ProjectWindow(project);
    pw->showMaximized();
    QObject::connect(pw, SIGNAL(closed()), this, SLOT(slotProjectWindowClosed()));

    this->setEnabled(false);
    this->setHidden(true);

}

void EventWindow::slotProjectWindowClosed()
{
    _eventsWidget->updateTreeView();
    this->setEnabled(true);
    this->setHidden(false);

}

void EventWindow::slotProjectSelected(Project& project)
{
    slotInterfaceModifProject(project);
}

void EventWindow::slotTaskSelected(Task& task)
{
    slotInterfaceModifTask(task);
}

void EventWindow::slotEventSelected(Event&e){
    EventTraditionnal* et = dynamic_cast<EventTraditionnal*>(&e);
    EventTask* eta = dynamic_cast<EventTask*>(&e);
    if(et) slotInterfaceModifEvtTrad(*et);
    else if(eta)emit signalChEvtTask(*eta);
    else throw("error type");

    QDate date = e.getDateDebut().date();
    _agenda->setWeekDate(date);
    _eventsWidget->setCalendarDate(date);

}


