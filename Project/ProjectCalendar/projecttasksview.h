#ifndef PROJECTTASKSVIEW_H
#define PROJECTTASKSVIEW_H

#include <QDebug>
#include <QMap>

#include <QWidget>
#include <QMenu>
#include <QTreeView>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTreeWidgetItem>
#include <QStandardItem>
#include "calendar.h"

/*!
 * \brief Left Dock de la fenêtre de Projet. Contient le tree view du projet.
 */
class ProjectTasksView : public QWidget
{
    Q_OBJECT
public:
    explicit ProjectTasksView(Project& project, QWidget *parent = 0);

    void updateWidget();
    void deleteTask(const Task& task);

signals:
    void   taskSelected(Task* task);
    void   taskAdded(Task& task);

    void   projectSelected();

public slots:
    void	indexSelected(const QModelIndex & index);
    void    newUnitTaskButtonClicked(bool);
    void    newCompositeTaskButtonClicked(bool);
    void    projectClicked(bool);



private:



    void addTask(bool isComposite, CompositeTask *father);
    CompositeTask* getSelectedFather();
    QStandardItem* getTreeStandardItem(const UnitTask& task);
    QStandardItem* getTreeStandardItem(const CompositeTask& task);


    Project& _project;
    Task*          _selectedTask;

    QPushButton *_projectButton;
    QTreeView* _tasksTreeView;
    QStandardItemModel* _tasksModel;
    QMap<const Task*, QStandardItem*>               _taskItemMap;



};

#endif // PROJECTTASKSVIEW_H
