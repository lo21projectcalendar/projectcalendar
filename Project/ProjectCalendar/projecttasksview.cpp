#include "projecttasksview.h"


ProjectTasksView::ProjectTasksView(Project& project, QWidget *parent) : QWidget(parent), _project(project), _selectedTask(0)
{


    this->setMinimumWidth(300);


    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->setMargin(0);



    _projectButton = new QPushButton(_project.getName());
    QObject::connect(_projectButton, SIGNAL(clicked(bool)), this, SLOT(projectClicked(bool)));
    vLayout->addWidget(_projectButton);




    _tasksTreeView = new QTreeView(this);
    _tasksTreeView->header()->hide();
    _tasksTreeView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QObject::connect(_tasksTreeView, SIGNAL(clicked(QModelIndex)), this, SLOT(indexSelected(QModelIndex)));
    vLayout->addWidget(_tasksTreeView);







    QPushButton *newUnitTaskButton = new QPushButton("New ...");

    QMenu *tasksMenu = new QMenu();

    QAction *newTaskAction = new QAction("New Task", tasksMenu);
    QObject::connect(newTaskAction, SIGNAL(triggered(bool)), this, SLOT(newUnitTaskButtonClicked(bool)));
    tasksMenu->addAction(newTaskAction);

    QAction *newCompositeTaskAction = new QAction("New Composite Task", tasksMenu);
    QObject::connect(newCompositeTaskAction, SIGNAL(triggered(bool)), this, SLOT(newCompositeTaskButtonClicked(bool)));
    tasksMenu->addAction(newCompositeTaskAction);
    newUnitTaskButton->setMenu(tasksMenu);

    vLayout->addWidget(newUnitTaskButton);

    setLayout(vLayout);

    updateWidget();

}



void ProjectTasksView::updateWidget()
{

    _projectButton->setText(_project.getName());
    _tasksModel = new QStandardItemModel;


    TaskManager &tm = TaskManager::getInstance();

    for (TaskManager::Iterator it = tm.getRootIteratorForProjectAndRootTask(_project, 0); !it.isDone(); it.next())
    {
        QStandardItem *item = 0;

        if  ( UnitTask *ut = dynamic_cast<UnitTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ut);

        }
        else if ( CompositeTask *ct = dynamic_cast<CompositeTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ct);

        }



        _tasksModel->appendRow(item);
    }
    _tasksTreeView->setModel(_tasksModel);
    _selectedTask = 0;

}


QStandardItem* ProjectTasksView::getTreeStandardItem(const UnitTask& task)
{
    QStandardItem *standardItem = new QStandardItem;

    QString text =  "";

    if (task.isFullyScheduled())
        text.append("◉ - ");
    else if (!task.canBeScheduled())
        text.append("◎ - ");
    else
        text.append("⦿ - ");


    text.append(task.getTitle());

    standardItem->setText(text);
    standardItem->setData(task.getId());
    if (task.isPreemptive())
        standardItem->setIcon(QIcon(":/images/preemptive_task.png"));
    else
        standardItem->setIcon(QIcon(":/images/task.png"));




     _taskItemMap[&task] = standardItem;

    return standardItem;
}

QStandardItem* ProjectTasksView::getTreeStandardItem(const CompositeTask& task)
{
    QStandardItem *standardItem = new QStandardItem;

    QString text =  "";

    if (task.isFullyScheduled())
        text.append("◉ - ");
    else if (!task.canBeScheduled())
        text.append("◎ - ");
    else
        text.append("⦿ - ");


    text.append(task.getTitle());

    standardItem->setText(text);
    standardItem->setData(task.getId());
    standardItem->setIcon(QIcon(":/images/folder.png"));

    _taskItemMap[&task] = standardItem;


    for (TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(task.getProject(), &task); !it.isDone(); it.next())
    {

        QStandardItem *item = 0;
        if  ( UnitTask *ut = dynamic_cast<UnitTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ut);

        }
        else if ( CompositeTask *ct = dynamic_cast<CompositeTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ct);

        }
        standardItem->appendRow(item);
    }
    return standardItem;
}



void ProjectTasksView::indexSelected(const QModelIndex &index)
{
    qDebug() << "Click " << index.data(Qt::UserRole+1).toString();

    const QString& id = index.data(Qt::UserRole+1).toString();

    _selectedTask = &TaskManager::getInstance().getTask(id, _project);


    emit taskSelected(_selectedTask);

}

CompositeTask* ProjectTasksView::getSelectedFather()
{
    CompositeTask *father = 0;

    if (_selectedTask)
    {

        if (CompositeTask* ct = dynamic_cast<CompositeTask*>(_selectedTask))
        {
            father = ct;
        }
        else
        {
            father = _selectedTask->getFather();

        }


    }

    return father;

}


void ProjectTasksView::addTask(bool isComposite, CompositeTask *father)
{
    Task* newTask;

    QString randomID = TaskManager::getInstance().generateID(_project);
    QStandardItem *newItem;
    if (!isComposite)
    {

        UnitTask* newUnitTask = &TaskManager::getInstance().addUnitTask(_project,randomID, "New Task", Duree(1,0), QDate::currentDate(), QDate::currentDate().addMonths(1));
        newItem = getTreeStandardItem(*newUnitTask);
        newTask = newUnitTask;



    }
    else
    {
        CompositeTask* newCompositeTask = &TaskManager::getInstance().addCompositeTask(_project, randomID, "New Composite Task");
        newItem = getTreeStandardItem(*newCompositeTask);
        newTask = newCompositeTask;
    }


    if (father)
    {
        CompositeManager::getInstance().addComposition(*father, *newTask);

        QStandardItem *fatherItem = _taskItemMap[father];
        fatherItem->appendRow(newItem);

    }
    else
    {
        _tasksModel->appendRow(newItem);
    }

     emit taskAdded(*newTask);




}

void ProjectTasksView::deleteTask(const Task &)
{

    updateWidget();
}



void ProjectTasksView::newUnitTaskButtonClicked(bool)
{
    CompositeTask* father = getSelectedFather();
    addTask(false, father);

}

void ProjectTasksView::newCompositeTaskButtonClicked(bool)
{
    CompositeTask* father = getSelectedFather();
   addTask(true, father);

}

void ProjectTasksView::projectClicked(bool)
{
    _tasksTreeView->clearSelection();
    emit projectSelected();
}
