#ifndef TASKDEPENDENCIESGRAPHWIDGET_H
#define TASKDEPENDENCIESGRAPHWIDGET_H

#include <QDebug>

#include <QWidget>
#include <QPushButton>
#include <QPainter>
#include <QPoint>
#include <QGridLayout>
#include <QObjectCleanupHandler>

#include <vector>
#include <list>
#include "calendar.h"

/*!
 * \brief Graphe affichant les tâches ainsi que les contraintes de précédence.
 */
class TaskDependenciesGraphWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TaskDependenciesGraphWidget(const Project &project, QWidget *parent = 0);

    void setRootTask(const CompositeTask* rootTask);
    void updateGraph();

signals:

    void taskSelected(Task* task);


public slots:

    void clicked();

private:

    QPoint                  getPointOfTask(const Task &task) const;


    const Project&                _project;
    const CompositeTask*    _rootTask;

    vector<QPushButton> _taskButtons;

    map<int, list <const Task*> > _matrix;
    map<QPushButton*, const Task*> _buttonTaskMap;

    void paintEvent(QPaintEvent *);
    QGridLayout *_gridLayout;


    int _width;
    int _xOffset;
    int _height;
    int _yOffset;

};

#endif // TASKDEPENDENCIESGRAPHWIDGET_H
