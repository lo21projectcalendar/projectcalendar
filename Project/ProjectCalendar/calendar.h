//
//  calendar.h




#ifndef __ProjectCalendar__calendar__
#define __ProjectCalendar__calendar__

#include <stdio.h>
#include <iostream>
#include <vector>
#include <QString>
#include <QDate>
#include <QTextStream>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QDebug>

#include "timing.h"

using namespace std;
using namespace TIME;

/*!
 *  \brief Classe gérant les exception de ce fichier
 */


class CalendarException{
public:
    CalendarException(const QString& message):info(message){}
    QString getInfo() const { return info; }
private:
    QString info;
};


/*!
 * \brief Strategie de sélection des éléments par l'itérateur. Par défaut, cette stratégie permet de sélectionenr tous les éléments du Manager.
 */



/*!
 * \brief Contener gérant une collection d'objets
 */
template <typename T>
class _Contener
{


   public:


    /*!
     * \brief Ajoute un objet au conteneur
     * \param t l'objet à ajouter
     */
    virtual void addObject(T& t) = 0;


    /*!
     * \brief Supprime un objet du content
     * \param t l'objet à supprimer
     */
    virtual void deleteObject(T& t) = 0;
    virtual ~_Contener() {}


    /*!
     * \brief Strategie de sélection des éléments. Par défaut, cette stratégie permet de sélectionenr tous les éléments.
     */


    class SelectionStrategy
    {
    public:
        SelectionStrategy() {}
        virtual ~SelectionStrategy() {}

        /*!
         * \brief Opérateur indiquant si l'élément est à sélectionner ou pas.
         * \param element Element du tableau
         * \return true Si l'élément est à sélectionner
         * \return false Sinon
         */
        virtual bool operator()(const T&) const {return true;}
    };


    /*!
     * \brief Strategie de suppression des éléments, appelée à chaque fois qu'un élément est supprimé. Par défaut, cette stratégie permet ne fait rien.
     */

    class DeleteRoutineStrategy
    {
    public:
        DeleteRoutineStrategy() {}
        virtual ~DeleteRoutineStrategy() {}

        /*!
         * \brief Opérateur effectuant la routine de suppression
         * \param t Element supprimé
         */

        virtual void operator()(const T&) const {}
    };


    /*!
     * \brief Itérateur abstrait
     */
    class _Iterator
    {
    public:
        _Iterator(SelectionStrategy *strategy);
        /*!
         * \brief Sélectionne le prochain élément. Soulève une exception si l'itérateur est terminé
         */
        virtual void next() = 0;

        /*!
         * \brief Détermine si l'itérateur est terminé
         * \return true Si l'itérateur est terminé
         * \return false sinon
         */
        virtual bool isDone() const = 0;

        /*!
         * \brief Retourne l'élément sélectionné par l'itérateur
         * \return L'élément sélectionné par l'Itérateur
         */

        virtual T& currentItem() const = 0;
    protected:

        /*!
         * \brief Vérifie qu'un élément répond bien à une stratégie de sélection
         * \param t Element à vérifier
         * \return true si t est conforme à la stratégie
         * \return false sinon
         */
        bool conformToStrategy(T& t) const {return _strategy->operator ()(t);}

    private:
        SelectionStrategy* _strategy;
    };



};

template <typename T>
_Contener<T>::_Iterator::_Iterator(SelectionStrategy *strategy) : _strategy(strategy) {}


template <typename T>
/*!
 * \brief Implémentation dun conteneur sous la forme d'un vecteur
 */
class Vector : public _Contener<T>
{
public:
    Vector();
    ~Vector();

    virtual void addObject(T& t);
    virtual void deleteObject(T& t);

    /*!
     * \brief Itérateur d'un Vector
     */
    class Iterator : public _Contener<T>::_Iterator
    {
    public:
        Iterator(typename _Contener<T>::SelectionStrategy *strategy, T** objects, unsigned int nbItems);
        void next();
        bool isDone() const;
        T& currentItem() const;
        virtual ~Iterator() {};

    private:
        T** _objects;
        unsigned int _nbItems;
        unsigned int _i;
    };


    template <class deleteselectionstrategy_type = typename _Contener<T>::SelectionStrategy, class deleteroutinestrategy_type = typename _Contener<T>::DeleteRoutineStrategy>
    void  deleteObjects(deleteselectionstrategy_type selectionStrategy = deleteselectionstrategy_type(), deleteroutinestrategy_type routineStrategy = deleteroutinestrategy_type());

    Iterator               iterator(typename _Contener<T>::SelectionStrategy* strategy = new typename _Contener<T>::SelectionStrategy()) const {return  Iterator(strategy, _objects, _nb);}



private:
    unsigned int             _nb;
    unsigned int             _nbMax;
    T**                      _objects;

};

template <typename T>
Vector<T>::Vector() : _nb(0), _nbMax(10), _objects(new T*[10]) {}

template <typename T>
Vector<T>::~Vector()
{
    for (unsigned int i = 0; i < _nb; ++i)
        delete _objects[i];

    _nb = 0;
}

template <typename T>
void Vector<T>::addObject(T &t)
{
    if (_nb == _nbMax)
    {

        _nbMax += 10;

        T **newObjects = new T*[_nbMax];

        for (unsigned int i = 0; i < _nb; i++)
            newObjects[i] = _objects[i];

        delete [] _objects;
        _objects = newObjects;

    }

    _objects[_nb++] = &t;
}


template <typename T>
void Vector<T>::deleteObject(T &t)
{
    T** p = _objects;

    unsigned int i = 0;

    while (*p != &t && i < _nb)
    {
        ++p;
        ++i;
    }

    if (i == _nb)
        throw CalendarException("Trying to delete an object which doesn't exist");

    delete &t;

    for (; i < _nb-1; ++i)
    {
         *p = *(p+1);
        ++p;
    }

     --_nb;

}

template <typename T>
template <class deleteselectionstrategy_type, class deleteroutinestrategy_type>
void    Vector<T>::deleteObjects(deleteselectionstrategy_type selectionStrategy, deleteroutinestrategy_type routineStrategy)
{
    for (unsigned int i = 0; i < _nb;)
    {
        if (!selectionStrategy(*_objects[i]))
        {
            ++i;
        }
        else
        {
            routineStrategy(*_objects[i]);
            deleteObject(*_objects[i]);
        }
    }
}


template <typename T>
Vector<T>::Iterator::Iterator(typename _Contener<T>::SelectionStrategy* strategy, T** objects, unsigned int nbItems) : _Contener<T>::_Iterator(strategy), _objects(objects), _nbItems(nbItems), _i(0)
{

    while (_i < _nbItems && !Iterator::conformToStrategy(*_objects[_i] ) )
    {
        _i++;
    }
}

template <typename T>
void Vector<T>::Iterator::next()
{
    if (isDone())
        throw CalendarException("Iterator is done");

    do
    {
       _i++;
    } while(_i < _nbItems && !Iterator::conformToStrategy(*_objects[_i]));

}

template <typename T>
bool Vector<T>::Iterator::isDone() const
{
    for (unsigned int i = _i; i < _nbItems; i++)
    {
        if (Iterator::conformToStrategy(*_objects[i]))
            return false;
    }

    return true;
}

template <typename T>
T& Vector<T>::Iterator::currentItem() const
{
    return *_objects[_i];
}




template <typename T>
/*!
 * \brief Adapter d'un conteneur
 */
class Manager
{

public:


    /*!
     * \brief Adapteur d'un Iterateur de Conteneur
     */
    class Iterator
    {

        friend class Manager<T>;

    public:


        bool isDone() const {return _iterator.isDone();};
        void next() {_iterator.next();};
        T& currentItem() const {return _iterator.currentItem();}

        virtual ~Iterator() {};

    protected:
        Iterator(typename Vector<T>::Iterator iterator);
        typename Vector<T>::Iterator _iterator;

    };

    /*!
     * \brief Adapteur const d'un Iterateur de Conteneur
     */
    class ConstIterator : public Iterator
    {

        friend class Manager<T>;

    public:

        const T& currentItem() const {return Iterator::currentItem();}

    private:
        ConstIterator(typename Vector<T>::Iterator iterator) : Iterator(iterator) {}


    };



    /*!
     * \brief
     * \param strategy Stratégie de sélection
     * \return Itérateur avec strategie
     */
    Iterator               iterator(typename _Contener<T>::SelectionStrategy* strategy = new typename _Contener<T>::SelectionStrategy()) {return  Iterator(_contener.iterator(strategy) );}

    /*!
     * \brief
     * \param strategy Stratégie de sélection
     * \return Itérateur constant avec strategie
     */
    ConstIterator          constIterator(typename _Contener<T>::SelectionStrategy* strategy = new typename _Contener<T>::SelectionStrategy()) const {return  ConstIterator(_contener.iterator(strategy));}





protected:

    Manager();
    virtual ~Manager();
    Manager(const Manager&);
    Manager& operator=(const Manager&);

    /*!
     * \brief Ajoute un objet au conteneur
     * \param t Objet à ajouter
     */
    void                    addObject(T& t);

    template <class selectionstrategy_type>
    /*!
     * \brief Recherche un objet
     * \param selectionStrategy Stratégie de selection
     * \return T* L'objet recherché s'il existe
     * \return 0 Sinon
     */
    T*                      findObject(selectionstrategy_type selectionStrategy = selectionstrategy_type()) const;

    /*!
     * \brief Supprime un objet du conteneur
     * \param t L'objet à supprimer
     */
    void                    deleteObject(T& t);


    template <class deleteselectionstrategy_type = typename _Contener<T>::SelectionStrategy, class deleteroutinestrategy_type = typename _Contener<T>::DeleteRoutineStrategy>

    /*!
     * \brief Supprime plusieurs objets du conteneur
     * \param selectionStrategy Stratégie de sélection
     * \param routineStrategy Routine de suppression
     */
    void deleteObjects(deleteselectionstrategy_type selectionStrategy = deleteselectionstrategy_type(), deleteroutinestrategy_type routineStrategy = deleteroutinestrategy_type());


    Vector<T>              _contener;


};

template <typename T>
Manager<T>::Manager()  {}

template <typename T>
Manager<T>::~Manager()
{
    _contener.~Vector();
}


template <typename T>
void Manager<T>::addObject(T& t)
{
    _contener.addObject(t);
}

template <typename T>
void Manager<T>::deleteObject(T& t)
{

    _contener.deleteObject(t);
}

template <typename T>
template <class selectionstrategy_type>
T*   Manager<T>::findObject(selectionstrategy_type selectionStrategy) const
{

    selectionstrategy_type *sel = new selectionstrategy_type(selectionStrategy);
    Iterator it = constIterator(sel);

   if (it.isDone())
       return 0;

   return &it.currentItem();


}



template <typename T>
template <class deleteselectionstrategy_type, class deleteroutinestrategy_type>
void Manager<T>:: deleteObjects(deleteselectionstrategy_type selectionStrategy, deleteroutinestrategy_type routineStrategy)
{

    _contener.deleteObjects(selectionStrategy, routineStrategy);


}

template <typename T>
Manager<T>::Iterator::Iterator(typename Vector<T>::Iterator iterator) :  _iterator(iterator)
{

}

/*!
 * \brief Projet
 */
class Project
{
    friend class Vector<Project>;
    friend class ProjectManager;
    
public:

    QString getName() const {return _name;}
    void setName(const QString& name);

    /*!
     * \brief Vérifie si le projet a été complètement programmé
     * \return true Si le projet a été complètement programmée
     * \return false Sinon
     */
    bool isFullyScheduled() const;


    /*!
     * \brief Récupère la borne supérieure des contraintes de disponibilités de toutes les tâches
     */
    QDate getAvailable() const;

    /*!
     * \brief Récupère la borne inférieure des contraintes de deadline de toutes les tâches
     */
    QDate getDeadline() const;



private:
    

    QString     _name;
    
    
    Project(const QString& name) : _name(name) {}
    ~Project() {};
    Project(const Project&);
    Project& operator=(const Project&);
    
    
};


/*!
 * \brief Manager de projet
 */
class ProjectManager : public Manager<Project> {

    friend class DataManager;
public:
    static ProjectManager& getInstance();

    /*!
     * \brief Créé et ajoute un projet.
     * \param name Le nom du projet à ajouter. Soulève une exception si un projet porte déjà ce nom.
     * \return Référence vers le nouveau projet
     */
    Project&             addProject(const QString& name);

    /*!
     * \brief Récupère un projet.
     * \param name Le nom du projet à récupérer. Soulève une exception si aucun projet ne porte ce nom
     * \return Référence vers le projet recherché
     */
    Project&             getProject(const QString& name);

    /*!
     * \brief Supprimer un projet
     * \param project Référence vers le projet à supprimer. Soulève une exception si ce projet n'existe pas.
     */
    void                 deleteProject(const Project& project);

    /*!
     * \brief Vérifie si le projet existe
     * \param name Le nom du projet
     * \return true Si le projet existe
     * \return false Sinon
     */
    bool                 containsProject(const QString& name);

    /*!
     * \brief Stratégie de sélection par le nom du projet
     */
    class ProjectNameStrategy : public _Contener<Project>::SelectionStrategy
    {

    public:
        ProjectNameStrategy(const QString& name) : SelectionStrategy(), _name(name) {}
        bool operator()(const Project& project) const {return project.getName() == _name; }

    private:
        const QString& _name;
    };

    /*!
     * \brief Routine de suppression supprimant tous les éléments d'un projet
     */
    class DeleteElementsRoutineStrategy : public _Contener<Project>::DeleteRoutineStrategy
    {

    public:
        DeleteElementsRoutineStrategy() : DeleteRoutineStrategy() {}
        void operator()(const Project& project) const;

    };


private:
    ProjectManager() : Manager<Project>::Manager() {}
    //Cette ligne marche sous Mac mais pas sous Windows
    //~ProjectManager() { Manager<Project>::~Manager(); }
    ProjectManager(const ProjectManager&);
    ProjectManager& operator=(const ProjectManager&);

};



class CompositeTask;
class EventTask;

/*!
 * \brief Tâche
 */
class Task {
    friend class Vector<Task>;
    friend class TaskManager;

public:
    
    QString             getId() const {return _id;};
    void                setId(const QString& id);

    QString             getTitle() const {return _title;};
    void                setTitle(const QString& title) {_title = title;}

    Project&            getProject() const {return _project;}

    virtual Duree       getDuration() const = 0;
    virtual QDate       getAvailable() const = 0;
    virtual QDate       getDeadline() const = 0;

    /*!
     * \brief Récupère la profondeur en termes de précédence
     * \return Profondeur
     */
    unsigned int        getPrecedenceDepth() const;

    /*!
     * \brief Récupère le père (CompositeTask) d'une tache s'il existe
     * \return Father Si cette tâche a un père
     * \return 0 Si cette tâche n'a pas de père
     */

    CompositeTask*               getFather() const;


    /*!
     * \brief Vérifie si la tâche est complètement programmée
     * \return true Si la tâche est complètement programmée
     * \return false Sinon
     */
    virtual bool        isFullyScheduled() const = 0;

    /*!
     * \brief Vérifie si une tâche peut être programmée
     * \return true Si la tâche est complètement programmée
     * \return false Sinon
     */
    virtual bool        canBeScheduled() const;

    /*!
     * \brief Vérifie si la tâche est partiellement programmée
     * \return true Si la tâche est partiellement ou complètement programmée
     * \return false Sinon
     */
    virtual bool        isPartiallyScheduled() const = 0;

    /*!
     * \brief Vérifie si la tâche peut être modifiée selon ses programmations et ses contraintes de précédences.
     * \return true Si la tâche peut être modifiée
     * \return false Sinon
     */
    virtual bool        canBeModified() const = 0;


    
protected:
    Task(Project& project, const QString& id, const QString& title):
    _project(project), _id(id), _title(title) {}
    virtual ~Task() {}

    Project& _project;
    QString _id;
    QString _title;
    
private:


    
    Task(const Task& t);
    Task& operator=(const Task&);
    

    
};

/*!
 * \brief Tâche non programmable qui peut être composée d'autres tâches
 */

class CompositeTask : public Task {
    
public:
    
    QDate      getAvailable() const;
    QDate      getDeadline() const;
    Duree      getDuration() const;
    bool        isFullyScheduled() const;
    bool        isPartiallyScheduled() const;

    bool        canBeModified() const;


    
private:
    
    friend class TaskManager;
    
    CompositeTask(Project &project, const QString& id, const QString& title) :
        Task(project, id, title)
        {}

};

/*!
 * \brief Tâche unitaire programmable
 */

class UnitTask : public Task {
    
public:
    QDate       getAvailable() const;
    QDate       getDeadline() const;
    Duree       getDuration() const;
    bool        isFullyScheduled() const;
    bool        isPartiallyScheduled() const;
    bool        canBeModified() const;

    /*!
     * \brief Récupère le temps de programmation restant
     */
    Duree       getScheduledTimeLeft() const;

    /*!
     * \brief Récupère le temps de programmation déjà programmé
     */
    Duree       getScheduledTimeDone() const;

    
    void                setAvailable(const QDate& available)
    {

        if (available > _deadline)
            throw CalendarException("Available date can't be after deadline");

        _available = available;
    }

    void                setDeadline(const QDate& deadline)
    {
        if (_available > deadline)
            throw CalendarException("Available date can't be after deadline");

        _deadline = deadline;
    }

    void                setDuration(const QTime& duration)
    {
        int nbMinutes = duration.minute() + 60*duration.hour();
        _duration = Duree(nbMinutes);
    }
    
    bool                isPreemptive() const { return _preemptive; }
    void                setPreemptive(bool preemptive) { _preemptive=preemptive; }
    


private:

    friend class TaskManager;

    UnitTask(Project &project, const QString& id, const QString& title, const Duree& duration, const QDate& available, const QDate& deadline, bool preemptive = false) :
        Task(project, id, title), _duration(duration), _available(available), _deadline(deadline), _preemptive(preemptive)
        {}

    virtual ~UnitTask() {}

    Duree   _duration;
    QDate   _available;
    QDate   _deadline;
    bool    _preemptive;
    
    
};



/*!
 * \brief Représente le lien entre 2 tâches
 */
class TaskLink
{
    
public:
    
protected:
    
    TaskLink(const Task& a, const Task& b) : _a( const_cast<Task&>(a)), _b(const_cast<Task&>(b)) {}

    Task& _a;
    Task& _b;
    
private:
    TaskLink(const TaskLink&);
    TaskLink& operator=(const TaskLink&);
};

/*!
 * \brief Contrainte de précédence implémentée en termes de lien entre 2 tâches
 */
class Precedence : private TaskLink
{

    friend class Vector<Precedence>;
    friend class PrecedenceManager;

public:
    
    Precedence(const Task& previous, const Task& next) : TaskLink(previous, next) {}

    Task& getPrevious() const {return _a;}
    Task& getNext() const {return _b;}

private:

    
};

/*!
 * \brief Contrainte de composition implémentée en termes de lien entre une CompositeTask et une Task
 */
class Composite : private TaskLink
{

    friend class Vector<Composite>;
    friend class CompositeManager;

public:
    
    Composite(const CompositeTask& father, const Task& son) : TaskLink(father, son) {}

    CompositeTask& getFather() const {return dynamic_cast<CompositeTask&>(_a);}
    Task& getSon() const {return _b;}
    
private:
    
    
};


/*!
 * \brief Manager de Task
 */
class TaskManager : public Manager<Task>
{
    friend class DataManager;

public:

    static TaskManager& getInstance();

    /*!
     * \brief Créé et ajoute une UnitTask
     * \param project Projet contenant la nouvelle tâche
     * \param id Id de la tâche. Soulève une exception si l'ID existe déjà.
     * \param title
     * \param duree
     * \param available
     * \param deadline
     * \param preemptive
     * \return Référence vers la nouvelle UnitTask
     */
    UnitTask&           addUnitTask(Project &project, const QString& id, const QString& title = "", const Duree& duree = Duree(0,0), const QDate& available = QDate(), const QDate& deadline = QDate(), bool preemptive = false);

    /*!
     * \brief Créé et ajoute une CompositeTask
     * \param project
     * \param id Id de la tâche. Soulève une exception si l'ID existe déjà.
     * \param title
     * \return Référence vers la nouvelle CompositeTask
     */
    CompositeTask&      addCompositeTask(Project &project, const QString& id, const QString& title = "");

    /*!
     * \brief Supprime une tache et ses contraintes de précédence et de composition
     * \param task La tâche à supprimer
     * \param fromFather true si le père l'a appelé (on suppose alors que c'est le père qui supprime la composition); false sinon.
     */
    void                deleteTask(Task& task, bool fromFather = false);
    
    /*!
     * \brief Récupère une tâche à partir de son id et de son projet. Soulève une exception si aucune tâche n'existe
     * \param id
     * \param project
     * \return Référence vers la tâche recherchée
     */
    Task&               getTask(const QString &id, const Project& project) const;

    /*!
     * \brief Vérifie si une tâche existe
     * \param id Id de la tâche
     * \param project
     * \return true Si la tâche existe
     * \return false Sinon
     */
    bool                containsTask(const QString &id, const Project& project);

    /*!
     * \brief Génère un ID unique pour une tâche d'un projet de la forme "%PROJECTNAME%_%RANDOMSTRING"
     * \param project
     * \return ID unique
     */
    QString             generateID(const Project &project) const;


    /*!
     * \brief Stratégie sélectionnant tous les éléments d'un projet
     */
    class ProjectIteratorStrategy : public _Contener<Task>::SelectionStrategy
    {

    public:
        ProjectIteratorStrategy(const Project& project) : SelectionStrategy(), _project(project) {}

        virtual bool operator()(const Task& task) const;

    private:
        const Project& _project;
    };
    /*!
     * \brief Stratégie sélectionnant les tâches ayant le même ID
     */
    class IDIteratorStrategy : public ProjectIteratorStrategy
    {
    public:
        IDIteratorStrategy(const Project& project, const QString& id) :ProjectIteratorStrategy(project), _id(id) {}

        bool operator()(const Task& task) const {return task.getId() == _id;};

    private:
        const QString&    _id;
    };

    /*!
     * \brief Stratégie sélectionnant les tâches à la racine, ou ayant un CompositeTask comme père
     */

    class RootIteratorStrategy : public ProjectIteratorStrategy
    {

    public:
        RootIteratorStrategy(const Project& project, const CompositeTask* rootTask) :ProjectIteratorStrategy(project), _rootTask(rootTask) {}

        bool operator()(const Task& task) const;

    private:
        const CompositeTask*    _rootTask;
    };

    /*!
     * \brief Stratégie sélectionnant les tâches programmables
     */
    class SchedulableTaskIteratorStrategy : public ProjectIteratorStrategy
    {

    public:
        SchedulableTaskIteratorStrategy(const Project& project) :ProjectIteratorStrategy(project) {}

        bool operator()(const Task& task) const;

    };

    /*!
     * \brief Stratégie sélectionnant les prédecesseurs d'une tâche
     */
    class PredecessorIteratorStrategy :  public ProjectIteratorStrategy
    {

    public:
        PredecessorIteratorStrategy(const Task& next) :  ProjectIteratorStrategy(next.getProject()), _next(next) {}

        bool operator()(const Task& task) const;

    private:
        const Task&    _next;
    };

    /*!
     * \brief Stratégie sélectionnant les tâches qui peuvent être avant une tâche
     */
    class PossiblePredecessorsIteratorStrategy :  public ProjectIteratorStrategy
    {

    public:
        PossiblePredecessorsIteratorStrategy(const Task& next) :  ProjectIteratorStrategy(next.getProject()), _next(next) {}

        bool operator()(const Task& task) const;

    private:
        const Task&    _next;
    };

    /*!
     * \brief Stratégie sélectionnant les successeurs d'une tâche
     */
    class SuccessorIteratorStrategy : public ProjectIteratorStrategy
    {

    public:
        SuccessorIteratorStrategy(const Task& previous) :  ProjectIteratorStrategy(previous.getProject()), _previous(previous) {}

        bool operator()(const Task& task) const;

    private:
        const Task&    _previous;
    };

    /*!
     * \brief Routine de suppression supprimant toutes les contraintes d'une tâche
     */
    class DeleteTasksRoutineStrategy : public _Contener<Task>::DeleteRoutineStrategy
    {
    public:
        DeleteTasksRoutineStrategy() : DeleteRoutineStrategy() {}
        void operator()(const Task& task) const;


    };

    void    deleteTasks(const Project& project)
    {
        deleteObjects(RootIteratorStrategy(project, 0), DeleteTasksRoutineStrategy());
    }

    Iterator            getIteratorForProject(const Project& project)
    {
        return iterator(new ProjectIteratorStrategy(project));

    }

    ConstIterator           getConstIteratorForProject(const Project& project) const
    {
        return constIterator(new ProjectIteratorStrategy(project));

    }

    Iterator            getRootIteratorForProjectAndRootTask(const Project& project, const CompositeTask* root)
    {
        return iterator(new RootIteratorStrategy(project, root));

    }

    ConstIterator            getConstRootIteratorForProjectAndRootTask(const Project& project, const CompositeTask* root) const
    {
        return constIterator(new RootIteratorStrategy(project, root));

    }

    Iterator            getPredecessorIteratorForTask(const Task& next)
    {
        return iterator(new PredecessorIteratorStrategy(next));

    }


    ConstIterator            getConstPredecessorIteratorForTask(const Task& next) const
    {
        return constIterator(new PredecessorIteratorStrategy(next));

    }

    Iterator            getPossiblePredecessorsIteratorForTask(const Task& next)
    {
        return iterator(new PossiblePredecessorsIteratorStrategy(next));

    }


    ConstIterator            getConstPossiblePredecessorsIteratorForTask(const Task& next) const
    {
        return constIterator(new PossiblePredecessorsIteratorStrategy(next));

    }


    Iterator            getSuccessorIteratorForTask(const Task& previous)
    {
        return iterator(new SuccessorIteratorStrategy(previous));

    }

    ConstIterator            getConstSuccessorIteratorForTask(const Task& previous) const
    {
        return constIterator(new SuccessorIteratorStrategy(previous));

    }

    Iterator            getSchedulableIteratorForTask(const Project& project)
    {
        return iterator(new SchedulableTaskIteratorStrategy(project));

    }

    ConstIterator            getConstSchedulableIteratorForTask(const Project& project) const
    {
        return constIterator(new SchedulableTaskIteratorStrategy(project));

    }

    
private:
    


    Task*           findTask(const QString &id, const Project &project) const;
    
    TaskManager() : Manager<Task>() {}
    //Cette ligne marche sous Mac mais pas sous Windows
    //~TaskManager() { Manager<Task>::~Manager(); }
    TaskManager(const TaskManager &tm);
    TaskManager& operator=(const TaskManager& tm);
    
};

/*!
 * \brief Manager de Precedence
 */
class PrecedenceManager : public Manager<Precedence> {
    
    friend class DataManager;

public:

    static PrecedenceManager& getInstance();

    /*!
     * \brief Ajoute une précédence. Vérifie que la précédence n'existe pas déjà, et qu'elle ne forme pas de circuit.
     * \param previous
     * \param next
     */
    void                    addPrecedence(const Task& previous, const Task& next);

    /*!
     * \brief Supprime toutes les contraintes de précédences d'une tâche
     * \param task
     */
    void                    deletePrecedencesRelatedToTask(const Task& task);

    /*!
     * \brief Supprime une contrainte particulière. Soulève une exception si la précédence n'existe pas
     * \param previous
     * \param next
     */
    void                    deletePrecedence(const Task& previous, const Task& next);

    /*!
     * \brief Vérifie si une précédence existe
     * \param previous
     * \param next
     * \return true Si la précédence existe
     * \return false Sinon
     */
    bool                    containsPrecedence(const Task& previous, const Task& next) const;

    /*!
     * \brief Vérifie si un chemin de précédence existe entre 2 taches
     * \param previous
     * \param next
     * \param checkEqual true si une égalité entre previous et next est considérée comme source de récursivité (utilisée que pour ContainsCycle)
     * \return true Si un chemin de précédence existe
     * \return false Sinon
     */
    bool                    containsRecursivePrecedence(const Task& previous, const Task& next, bool checkEqual = true) const;

    /*!
     * \brief Vérifie si un cycle de précédence existe
     * \param task
     * \return true Si un cycle de précédence a été trouvé
     * \return false Sinon
     */
    bool                    containsCycle(const Task& task) const;

    /*!
     * \brief Stratégie sélectionnant une précédence
     */
    class PrecedenceSelectionStrategy:public _Contener<Precedence>::SelectionStrategy
    {
    public:
        PrecedenceSelectionStrategy(const Task& previous, const Task& next) : SelectionStrategy(), _previous(previous), _next(next) {}
        bool operator()(const Precedence& precedence) const {return &precedence.getNext() == &_next && &precedence.getPrevious() == &_previous;}

    private:
        const Task& _previous;
        const Task& _next;

    };

    /*!
     * \brief Stratégie sélectionnant les contraintes liées à une tâche
     */
    class TaskLinkDeleteSelectionStrategy:public _Contener<Precedence>::SelectionStrategy
    {
    public:
        TaskLinkDeleteSelectionStrategy(const Task& task) : SelectionStrategy(), _task(task) {}
        bool operator()(const Precedence& precedence) const;

    private:
        const Task& _task;
    };


private:
    
    PrecedenceManager() : Manager<Precedence>() {}
    //Cette ligne marche sous Mac mais pas sous Windows
    //~PrecedenceManager() {Manager<Precedence>::~Manager();}
    PrecedenceManager(const PrecedenceManager&);
    PrecedenceManager& operator=(const PrecedenceManager&);
};

/*!
 * \brief Manager de Composite
 */

class CompositeManager : public Manager<Composite>{
    
    friend class DataManager;


public:

    static CompositeManager& getInstance();

    /*!
     * \brief Ajoute une contraite de composition. Soulève une exception si la composition existe déjà.
     * \param father
     * \param son
     */
    void                    addComposition(CompositeTask& father, Task& son);

    /*!
     * \brief Supprime la contrainte de composition
     * \param task Le fils de la composition
     */
    void                    deleteCompositionRelatedToTask(const Task& task);

    /*!
     * \brief Supprime la composition et toutes les sous-tâches d'une tâche composite
     * \param task
     */
    void                    deleteCompositionAndSubtasks(const CompositeTask& task);

    /*!
     * \brief Vérifie si une composition existe déjà.
     * \param father
     * \param son
     * \return true Si la composition existe déjà
     * \reutrn false Sinon
     */
    bool                    containsComposition(const CompositeTask& father, const Task& son) const;

    /*!
     * \brief Vérifie si un chemin de composition existe entre deux tâches
     * \param ancestor
     * \param son
     * \return true Si un chemin de compositon existe déjà
     * \return false Sinon
     */
    bool                    containsRecursiveComposition(const CompositeTask& ancestor, const Task& son) const;

    /*!
     * \brief Renvoie le père d'une tâche s'il existe
     * \param son
     * \return Father S'il existe
     * \return 0 Sinon
     */
    CompositeTask*          getFatherOf(const Task& son) const;
    

    /*!
     * \brief Stratégie sélectionnant toutes les Compositions ayant "father" comme père.
     */
    class FatherToSonSelectionStrategy:public _Contener<Composite>::SelectionStrategy
    {
    public:
        FatherToSonSelectionStrategy(const CompositeTask& father) : SelectionStrategy(), _father(father) {}
        virtual bool operator()(const Composite& composite) const {return &composite.getFather() == &_father;}

    private:
        const CompositeTask& _father;
    };

    /*!
     * \brief Stratégie sélectionnant toutes les Compositions ayant "son" comme fils.
     */
    class SonToFatherSelectionStrategy:public _Contener<Composite>::SelectionStrategy
    {
    public:
        SonToFatherSelectionStrategy(const Task& son) : SelectionStrategy(), _son(son) {}
        virtual bool operator()(const Composite& composite) const {return &composite.getSon() == &_son;}

    private:
        const Task& _son;
    };

    /*!
     * \brief Stratégie sélectionnant toutes les Compositions ayant "father" comme père et "son" comme fils.
     */
    class SonFatherSelectionStrategy : public _Contener<Composite>::SelectionStrategy
    {
    public:
        SonFatherSelectionStrategy(const Task& son, const CompositeTask& father) : SelectionStrategy(), _son(son), _father(father) {}
        bool operator()(const Composite& composite) const {return &composite.getSon() == &_son && &composite.getFather() == &_father;}
    private:
        const Task& _son;
        const CompositeTask& _father;

    };

    /*!
     * \brief Routine de suppression supprimant récursivement toutes les tâches à l'intérieure d'une tâche composite.
     */
    class RecursiveDeleteRoutineStrategy:public _Contener<Composite>::DeleteRoutineStrategy
    {
    public:
        RecursiveDeleteRoutineStrategy(const CompositeTask& task) : DeleteRoutineStrategy(), _task(task) {}
        void operator()(const Composite& composite) const;

    private:
        const CompositeTask& _task;
    };


private:
    

    
    CompositeManager() : Manager<Composite>() {}
    //Cette ligne marche sous Mac mais pas sous Windows
    //~CompositeManager() {Manager<Composite>::~Manager();}
    CompositeManager(const CompositeManager&);
    CompositeManager& operator=(const CompositeManager&);
};


#include <QDateTimeEdit>

/*!
 * \brief Evènement
 */
class Event{
    friend class Vector<Event>;
    friend class EventManager;
    public:
        Event(const QString& name,QDateTime dateDebut,QDateTime dateFin):_nameE(name),_dateDebutE(dateDebut),_dateFinE(dateFin){}
        virtual ~Event(){}
        virtual QString getName() const=0;
        QDateTime getDateDebut() const {return _dateDebutE;}
        QDateTime getDateFin() const {return _dateFinE;}
        Duree getDuration() const
        {
            unsigned int nbMinutes =  _dateDebutE.secsTo(_dateFinE)/60;
            return Duree(nbMinutes/60, nbMinutes%60);
        }

    protected:
        QString _nameE;
    private:
        QDateTime _dateDebutE;
        QDateTime _dateFinE;
};

/*!
 * \brief Evènement Traditionnel
 */
class EventTraditionnal: public Event{
   public:
    EventTraditionnal(const QString& name,const QDateTime dateDebut,const QDateTime dateFin, int nb,QString de):Event(name,dateDebut,dateFin),_nbPersonne(nb),_descriptionE(de){}
    ~EventTraditionnal(){}
    QString getName()const{return _nameE;}
    QString getDescription() const {return _descriptionE;}
    int getNbpersonne()const{return _nbPersonne;}
   private:
    int _nbPersonne;
    QString _descriptionE;
};

/*!
 * \brief Programmation d'une UnitTask
 */
class EventTask: public Event{
   public:
    EventTask(const QString& name,const QDateTime& dateDebut,const QDateTime& dateFin, const UnitTask& t):Event(name,dateDebut,dateFin),_taskE(t){}
    ~EventTask(){}
    QString getName()const{return _nameE;}
    const UnitTask& getTask() const {return _taskE;}


   private:
    const UnitTask& _taskE;
};

/*!
 * \brief Manager d'Event : gere les ajouts des evenements (Programmation dans le cas de taches)
 */
class EventManager:public Manager<Event> {
friend class DataManager;
public:

    /*!
     * \brief Permet de recuperer l'unique instance EventManager
     * \return L'unique EventManager
     */
    static EventManager& getInstance();

    /*!
     * \brief Créé et ajoute un évènement traditionnel
     * \param name
     * \param dateDebut
     * \param dateFin
     * \param nbPersonne
     * \param description
     * \return L'evenement traditionnel ajoute a l'Eventmanager
     */
    EventTraditionnal& addEventTrad(const QString &name, const QDateTime &dateDebut, const QDateTime &dateFin, int nbPersonne, QString description);

    /*!
     * \brief Créé et ajoute une programmation d'une tâche
     * \param name
     * \param dateDebut
     * \param dateFin
     * \param t
     * \return L'evenement task ajoute a l'Eventmanager
     */
    EventTask& addEventTask(const QString &name, const QDateTime &dateDebut, const QDateTime &dateFin, const UnitTask &t);

    /*!
     * \brief Verifie si un evenent Traditionnel peut etre programme ou non avant de l'ajouter au manager avec AddEvent
     * \param dateDebut
     * \param dateFin
     * \param nbPersonne
     * \return vrai ou faux
     */
    bool isValid(const QDateTime& dateDebut, const QDateTime& dateFin, int nbPersonne);

    /*!
     * \brief Verifie si un evenent Task peut etre programme ou non avant de l'ajouter au manager avec AddEvent
     *                Vefifie donc les contraintes de la tache a programmer
     * \param dateDebut
     * \param dateFin
     * \param t
     * \return vrai ou faux
     */
    bool isValid(const QDateTime &dateDebut, const QDateTime &dateFin, UnitTask* t);

    /*!
     * \brief Verifie si l'evenement place en parametre peut etre modifie ou non
     * \param dateDebut
     * \param dateFin
     * \param nbPersonne
     * \param et
     * \return vrai ou faux
     */
    bool isModifValid(const QDateTime& dateDebut, const QDateTime& dateFin, int nbPersonne, const QDateTime &oldDateDebut, const QDateTime &oldDateFin);
    bool isModifValid(const QDateTime &dateDebut, const QDateTime &dateFin, UnitTask* t, const EventTask& eta);

    /*!
     * \brief Récupère un évènement ayant ce nom. Soulève une exception si l'élèment n'existe pas.
     * \param name
     * \return L'evenement designe par le nom en parametre, leve une CalendarExeption sinon
     */
    Event& getEvent(const QString& name);



    /*!
     * \brief Stratégie de sélection par nom
     */
    class EventNameStrategy : public _Contener<Event>::SelectionStrategy

    {

    public:
        EventNameStrategy(const QString& name) : SelectionStrategy(), _name(name) {}
        bool operator()(const Event& event) const {return event.getName() == _name; }

    private:
        const QString& _name;
    };

    /*!
     * \brief Stratégie sélectionnant tous les évènements traditionnels
     */
    class EventTraditionnalSelectionStrategy : public _Contener<Event>::SelectionStrategy
    {
    public:
        EventTraditionnalSelectionStrategy() : SelectionStrategy() {}

        bool operator()(const Event& event) const { return dynamic_cast<const EventTraditionnal*>(&event); }
    };

    /*!
     * \brief Stratégie sélectionnant toutes les programmations
     */
    class EventTaskSelectionStrategy : public _Contener<Event>::SelectionStrategy
    {
    public:
        EventTaskSelectionStrategy() : SelectionStrategy() {}

        bool operator()(const Event& event) const { return dynamic_cast<const EventTask*>(&event); }
    };

    /*!
     * \brief Stratégie sélectionnant toutes les programmations d'une tâche
     */
    class EventTaskRelatedSelectionStrategy : public _Contener<Event>::SelectionStrategy
    {
    public:
        EventTaskRelatedSelectionStrategy(const UnitTask& task) : SelectionStrategy(), _task(task) {}

        bool operator()(const Event& event) const
        {
            if (const EventTask *et = dynamic_cast<const EventTask*>(&event))
            {
                return &et->getTask() == &_task;
            }
            else
            {
                return false;
            }

        }
    private:
        const UnitTask& _task;
    };

    /*!
     * \brief Supprime les évènements d'une tâche
     * \param task
     */
    void deleteEvents(const UnitTask& task)
    {
        deleteObjects(EventTaskRelatedSelectionStrategy(task), _Contener<Event>::DeleteRoutineStrategy());
    }

    Iterator getEventTraditionnalIterator() {
        return iterator(new EventTraditionnalSelectionStrategy());
    }

    ConstIterator getEventTraditionnalConstIterator() {
        return constIterator(new EventTraditionnalSelectionStrategy());
    }

    Iterator getEventTaskIterator() {
        return iterator(new EventTaskSelectionStrategy());
    }

    ConstIterator getEventTaskConstIterator() {
        return constIterator(new EventTaskSelectionStrategy());
    }

    Iterator getEventTaskRelatedIterator(const UnitTask& task) {
        return iterator(new EventTaskRelatedSelectionStrategy(task));
    }

    ConstIterator getEventTaskRelatedConstIterator(const UnitTask& task) {
        return constIterator(new EventTaskRelatedSelectionStrategy(task));
    }


private:
    EventManager() : Manager<Event>::Manager() {}
    //Cette ligne marche sous Mac mais pas sous Windows
    //~EventManager() {Manager<Event>::~Manager();}
    EventManager(const EventManager&);
    EventManager& operator=(const EventManager&);
};

/*!
 * \brief Manager en charge du cycle de vie de tous les autres managers
 */
class DataManager
{

public:


    /*!
     * \brief Stratégie de sauvegarde d'un fichier sur le disque dur
     */
    class DataPostDriveFileStrategy
    {
    public:
        bool operator()(QByteArray byteArray);
    };




    /*!
     * \brief Stratégie de création d'un fichier XML représentant les données
     */
    class DataSaveXMLStrategy
    {
    public:
        QByteArray operator()();
    };


    /*!
     * \brief Stratégie de lecture d'un fichier depuis le disque dur
     */
    class DataGetDriveFileStrategy
    {
    public:
        QByteArray operator()();

    };


    /*!
     * \brief Stratégie de chargement d'un fichier XML représentant les données
     */
    class DataLoadXMLStrategy
    {
    public:
        void operator()(QByteArray byteArray);

    private:
        QXmlStreamReader *_reader;

        void readData();

        void readProjects();
        void readProject();

        void readTasks(Project& project);

        void readUnitTask(Project& project);
        void readCompositeTask(Project& project);

        void readCompositions(Project& project);
        void readComposition(Project& project);

        void readPrecedences(Project& project);
        void readPrecedence(Project& project);

        void readEventsTask(const UnitTask& task);
        void readEventTask(const UnitTask& task);

        void readEventsTraditional();
        void readEventTraditional();


    };


    static void freeHandler();

    static ProjectManager& getProjectManager();
    static TaskManager& getTaskManager();
    static CompositeManager& getCompositeManager();
    static PrecedenceManager& getPrecedenceManager();
    static EventManager& getEventManager();


    template <class savestrategy_type, class poststrategy_type>
    /*!
     * \brief Sauvegarde toutes les données
     * \param saveStrategy Stratégie de sauvegarde en fichier (XML, PDF, ...)
     * \param postStrategy Stratégie de sauvegarde sur un support (Disque Dur, Internet, ...)
     */
    static void saveData(savestrategy_type saveStrategy = savestrategy_type(), poststrategy_type postStrategy = poststrategy_type());

    template <class getstrategy_type, class loadstrategy_type>
    /*!
     * \brief Charge des données
     * \param getStrategy Stratégie de récupération depuis un support (Disque Dur, Internet, ...)
     * \param loadStrategy Stratégie de chargement depuis un fichier (XML, Text, ...)
     */
    static void loadData(getstrategy_type getStrategy = getstrategy_type(), loadstrategy_type loadStrategy = loadstrategy_type());



private:
    /*!
     * \brief Handler des singletons. Tous les Managers sont composés ici.
     */
    class SingletonHandler
    {
        friend class DataManager;

       private:

        SingletonHandler() {}
        ~SingletonHandler() {

            _projectManager.~ProjectManager();
            _taskManager.~TaskManager();
            _compositeManager.~CompositeManager();
            _precedenceManager.~PrecedenceManager();
            _eventManager.~EventManager();

        }

        SingletonHandler(const SingletonHandler&);
        SingletonHandler& operator=(const SingletonHandler&);



        ProjectManager     _projectManager;
        TaskManager        _taskManager;
        CompositeManager   _compositeManager;
        PrecedenceManager  _precedenceManager;
        EventManager       _eventManager;

    };

    static SingletonHandler handler;

};


template <class savestrategy_type, class poststrategy_type>
void DataManager::saveData(savestrategy_type saveStrategy, poststrategy_type postStrategy)
{

    QByteArray array = saveStrategy();



    postStrategy(array);
}

template <class getstrategy_type, class loadstrategy_type>
void DataManager::loadData(getstrategy_type getStrategy, loadstrategy_type loadStrategy)
{

    QByteArray array = getStrategy();

    freeHandler();

    loadStrategy(array);
}



#endif /* defined(__ProjectCalendar__calendar__) */
