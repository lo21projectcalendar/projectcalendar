#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QMessageBox>
#include "projectwindow.h"
#include "eventwindow.h"
#include "agendawidget.h"
#include "projecteventswidget.h"

class Application : public QApplication
{
public:
    Application(int &argc, char **argv, int = ApplicationFlags ) : QApplication(argc, argv) {}

private:
    bool notify(QObject *, QEvent *);

};

bool Application::notify(QObject* receiver, QEvent* event)
{
    try {
        return QApplication::notify( receiver, event );
    } catch ( CalendarException e ) {

        QMessageBox::information(activeWindow(), "Error", e.getInfo());

        return false;
    }

}

int main(int argc, char *argv[])
{
    Application a(argc, argv);


    EventWindow* ew = new EventWindow();
    ew->showMaximized();


    int execValue;
try{
        execValue = a.exec();
    }
    catch(CalendarException e)
    {
        QMessageBox::information(ew, "Calendar Exception", e.getInfo());
    }
    return execValue;


}
