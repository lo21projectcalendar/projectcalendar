#ifndef AGENDAWIDGET_H
#define AGENDAWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include "eventdaywidget.h"
#include "agendatimelinewidget.h"

/*!
 * \brief Agenda hebdomadaire
 */
class AgendaWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AgendaWidget(QDate week, unsigned int scale = 30, QWidget *parent = 0);

signals:

public slots:

public:
    void    setWeekDate(QDate week);
    void    updateAgenda();

private:


    QDate _week;
    QGridLayout* _gridLayout;
    unsigned int _scale;
};

#endif // AGENDAWIDGET_H
