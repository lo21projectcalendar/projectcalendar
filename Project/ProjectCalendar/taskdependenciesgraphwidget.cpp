#include "taskdependenciesgraphwidget.h"


TaskDependenciesGraphWidget::TaskDependenciesGraphWidget(const Project &project, QWidget *parent) : QWidget(parent), _project(project), _rootTask(0), _gridLayout(0)
{
    _width = 50;
    _xOffset = 50;
    _height = 50;
    _yOffset = 50;


}

void TaskDependenciesGraphWidget::updateGraph()
{
    _matrix.clear();
    _buttonTaskMap.clear();

    if (_gridLayout)
    {


        QLayoutItem * item;
        QLayout * sublayout;
        QWidget * widget;
        while ((item = layout()->takeAt(0))) {
            if ((sublayout = item->layout()) != 0) {/* do the same for sublayout*/}
            else if ((widget = item->widget()) != 0) {widget->hide(); delete widget;}
            else {delete item;}
        }

        delete _gridLayout;

    }
    _gridLayout = new QGridLayout;


    TaskManager &tm = TaskManager::getInstance();


    for (TaskManager::ConstIterator rootIt = tm.getConstRootIteratorForProjectAndRootTask(_project, _rootTask); !rootIt.isDone();rootIt.next())
    {
        unsigned int depth = rootIt.currentItem().getPrecedenceDepth();
        _matrix[depth].push_back(&rootIt.currentItem());
    }





    for (TaskManager::Iterator rootIt = tm.getRootIteratorForProjectAndRootTask(_project, _rootTask); !rootIt.isDone();rootIt.next())
    {
        QPoint point = getPointOfTask(rootIt.currentItem());



        QString text = "";
        if (rootIt.currentItem().isFullyScheduled())
            text.append("◉ - ");
        else if (!rootIt.currentItem().canBeScheduled())
            text.append("◎ - ");
        else
            text.append("⦿ - ");


        text.append(rootIt.currentItem().getTitle());

        QPushButton *button = new QPushButton(text, this);








        if (dynamic_cast<CompositeTask*>(&rootIt.currentItem()))
        {
            button->setIcon(QIcon(":/images/folder.png"));
        }
        else if (UnitTask* ut = dynamic_cast<UnitTask*>(&rootIt.currentItem()))
        {
            if (ut->isPreemptive())
                button->setIcon(QIcon(":/images/preemptive_task.png"));
            else
                button->setIcon(QIcon(":/images/task.png"));
        }

        QObject::connect(button, SIGNAL(pressed()), this, SLOT(clicked()));

         _gridLayout->addWidget(button, point.x(), point.y());
         _buttonTaskMap[button] = &(rootIt.currentItem());


    }



    this->setLayout(_gridLayout);
    this->update();

}

void TaskDependenciesGraphWidget::setRootTask(const CompositeTask *rootTask)
{

    _rootTask = rootTask;
   updateGraph();
}

QPoint TaskDependenciesGraphWidget::getPointOfTask(const Task &task) const
{

    map<int, list<const Task*> > matrix = _matrix;
    map<int, list<const Task*> >::iterator it;
    list <const Task*>::iterator ii;

    for (it = matrix.begin(); it != matrix.end(); ++it)
    {
        unsigned int depth = it->first;

        int i = 0;
        for (ii = it->second.begin(); ii != it->second.end(); ++ii)
        {
            const Task *t = *(ii);

            if (t == &task)
                return QPoint(depth, i);

            i++;
        }



    }

    return QPoint();

}

void TaskDependenciesGraphWidget::paintEvent(QPaintEvent *)
{

    TaskManager &tm = TaskManager::getInstance();

    QPainter painter(this);

    QRect fatherRect, sonRect;
    int x1, x2, y1, y2;

    for (TaskManager::Iterator rootIt = tm.getRootIteratorForProjectAndRootTask(_project, _rootTask); !rootIt.isDone();rootIt.next())
    {
        QPoint point = getPointOfTask(rootIt.currentItem());
        sonRect = _gridLayout->cellRect(point.x(), point.y());


        for (TaskManager::Iterator predIt = tm.getPredecessorIteratorForTask(rootIt.currentItem()); !predIt.isDone();predIt.next())
        {
            QPoint fatherPoint = getPointOfTask(predIt.currentItem());

            fatherRect = _gridLayout->cellRect(fatherPoint.x(), fatherPoint.y());

            x1 = sonRect.x() + sonRect.width()/2;
            x2 = fatherRect.x() + fatherRect.width()/2;

            y1 = sonRect.y() + sonRect.height()/2;
            y2 =  fatherRect.y() + fatherRect.height()/2;

           painter.drawLine(x1,  y1, x2 , y2);

        }

    }

}


void TaskDependenciesGraphWidget::clicked()
{

    for (map<QPushButton*, const Task*>::iterator it = _buttonTaskMap.begin(); it != _buttonTaskMap.end(); ++it)
    {
        if ( ((QPushButton*)it->first)->isDown())
        {

            emit taskSelected(const_cast<Task*>(it->second));
            break;

        }
    }

}
