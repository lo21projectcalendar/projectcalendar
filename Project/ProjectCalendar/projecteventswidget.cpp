#include "projecteventswidget.h"

ProjectEventsWidget::ProjectEventsWidget(QWidget *parent) : QWidget(parent)
{

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(0);

    _calendar = new QCalendarWidget();
    _calendar->setFixedHeight(200);

    QObject::connect(_calendar, SIGNAL(clicked(QDate)), this, SLOT(slotDateSelected(QDate)));

    layout->addWidget(_calendar);


    _tasksTreeView = new QTreeView(this);
    _tasksTreeView->header()->hide();
    _tasksTreeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    _tasksTreeView->setExpandsOnDoubleClick(false);

    QObject::connect(_tasksTreeView, SIGNAL(clicked(QModelIndex)), this, SLOT(slotTreeViewClicked(QModelIndex)));
    QObject::connect(_tasksTreeView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(slotTreeViewDoubleClicked(QModelIndex)));



    layout->addWidget(_tasksTreeView);

    _eventsTreeView = new QTreeView(this);
    _eventsTreeView->header()->hide();
    _eventsTreeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    QObject::connect(_eventsTreeView, SIGNAL(clicked(QModelIndex)), this, SLOT(slotTreeViewClicked(QModelIndex)));
    QObject::connect(_eventsTreeView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(slotTreeViewDoubleClicked(QModelIndex)));

    layout->addWidget(_eventsTreeView);




    setLayout(layout);


    updateTreeView();
}


void ProjectEventsWidget::updateTreeView()
{
    updateTasksTreeView();
    updateEventsTreeView();

}

void ProjectEventsWidget::setCalendarDate(QDate date)
{
    _calendar->setSelectedDate(date);
}

void ProjectEventsWidget::updateTasksTreeView()
{
    QStandardItemModel *model = new QStandardItemModel;


    ProjectManager &pm = ProjectManager::getInstance();

    for (ProjectManager::Iterator it = pm.iterator(); !it.isDone(); it.next())
    {
        QStandardItem *item = getTreeStandardItem(it.currentItem());
        model->appendRow(item);
    }

    _tasksTreeView->setModel(model);

}

void ProjectEventsWidget::updateEventsTreeView()
{
    QStandardItemModel *model = new QStandardItemModel;

    for (EventManager::ConstIterator it = EventManager::getInstance().getEventTraditionnalConstIterator(); !it.isDone(); it.next())
    {
        QStandardItem *item = getTreeStandardItem(dynamic_cast<const EventTraditionnal&>(it.currentItem()));
        model->appendRow(item);
    }

    _eventsTreeView->setModel(model);

}

QStandardItem* ProjectEventsWidget::getTreeStandardItem(const Event& event)
{
    QStandardItem *standardItem = new QStandardItem;


    standardItem->setText(event.getName());
    standardItem->setData(event.getName());
    standardItem->setData("Event", Qt::UserRole+2);
    standardItem->setIcon(QIcon(":/images/event.png"));


    return standardItem;
}

QStandardItem* ProjectEventsWidget::getTreeStandardItem(const Project& project)
{
    QStandardItem *standardItem = new QStandardItem;


    QString text =  "";

    if (project.isFullyScheduled())
        text.append("◉ - ");
    else
        text.append("⦿ - ");


    text.append(project.getName());

    standardItem->setText(text);
    standardItem->setData(project.getName());
    standardItem->setData("Project", Qt::UserRole+2);

    standardItem->setIcon(QIcon(":/images/project.png"));

    for (TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(project, 0); !it.isDone(); it.next())
    {

        QStandardItem *item = 0;
        if  ( UnitTask *ut = dynamic_cast<UnitTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ut);

        }
        else if ( CompositeTask *ct = dynamic_cast<CompositeTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ct);

        }
        standardItem->appendRow(item);
    }

    return standardItem;
}

QStandardItem* ProjectEventsWidget::getTreeStandardItem(const UnitTask& task)
{
    QStandardItem *standardItem = new QStandardItem;

    QString text =  "";

    if (task.isFullyScheduled())
        text.append("◉ - ");
    else if (!task.canBeScheduled())
        text.append("◎ - ");
    else
        text.append("⦿ - ");


    text.append(task.getTitle());

    standardItem->setText(text);
    standardItem->setData(task.getId());
    standardItem->setData("Task", Qt::UserRole+2);
    standardItem->setData(task.getProject().getName(), Qt::UserRole+3);

    if (task.isPreemptive())
        standardItem->setIcon(QIcon(":/images/preemptive_task.png"));
    else
        standardItem->setIcon(QIcon(":/images/task.png"));



    for (EventManager::ConstIterator it = EventManager::getInstance().getEventTaskRelatedConstIterator(task); !it.isDone(); it.next())
    {
        QStandardItem *item = getTreeStandardItem(it.currentItem());
        standardItem->appendRow(item);
    }


    return standardItem;
}

QStandardItem* ProjectEventsWidget::getTreeStandardItem(const CompositeTask& task)
{
    QStandardItem *standardItem = new QStandardItem;

    QString text =  "";

    if (task.isFullyScheduled())
        text.append("◉ - ");
    else if (!task.canBeScheduled())
        text.append("◎ - ");
    else
        text.append("⦿ - ");
    text.append(task.getTitle());

    standardItem->setText(text);
    standardItem->setData(task.getId());
    standardItem->setData("Task", Qt::UserRole+2);
    standardItem->setData(task.getProject().getName(), Qt::UserRole+3);
    standardItem->setIcon(QIcon(":/images/folder.png"));

    for (TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(task.getProject(), &task); !it.isDone(); it.next())
    {

        QStandardItem *item = 0;
        if  ( UnitTask *ut = dynamic_cast<UnitTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ut);

        }
        else if ( CompositeTask *ct = dynamic_cast<CompositeTask*>(&it.currentItem())  )
        {
            item = getTreeStandardItem(*ct);

        }
        standardItem->appendRow(item);
    }
    return standardItem;
}

void ProjectEventsWidget::slotDateSelected(QDate date)
{
    emit dateSelected(date);
}

void ProjectEventsWidget::slotTreeViewClicked(QModelIndex index)
{

    qDebug() << "Click " << index.data(Qt::UserRole+1).toString();


    const QString& type = index.data(Qt::UserRole+2).toString();

    if (type == "Project")
    {
        const QString& name = index.data(Qt::UserRole+1).toString();
        Project& project = ProjectManager::getInstance().getProject(name);
        emit projectSelected(project);
    }
    else if (type == "Task")
    {
        const QString& projectName = index.data(Qt::UserRole+3).toString();
        Project& project = ProjectManager::getInstance().getProject(projectName);
        const QString& taskID = index.data(Qt::UserRole+1).toString();

        Task& task = TaskManager::getInstance().getTask(taskID, project);
        emit taskSelected(task);
    }
    else if (type == "Event")
    {
        const QString& name = index.data(Qt::UserRole+1).toString();
        Event& event = EventManager::getInstance().getEvent(name);
        emit selectEvtToModif(event);
    }
}

void ProjectEventsWidget::slotTreeViewDoubleClicked(QModelIndex index)
{
    const QString& type = index.data(Qt::UserRole+2).toString();
    qDebug() << "Click " << index.data(Qt::UserRole+1).toString();

    if (type == "Project")
    {
        const QString& name = index.data(Qt::UserRole+1).toString();

        Project& project = ProjectManager::getInstance().getProject(name);

        emit projectDoubleClicked(project);
    }
    else if (type == "Task")
    {

    }

    else if (type == "Event")
    {
        const QString& name = index.data(Qt::UserRole+1).toString();
        Event& event = EventManager::getInstance().getEvent(name);
        emit selectEvtToModif(event);
    }
}
