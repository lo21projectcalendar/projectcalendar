#ifndef PROJECTEVENTSWIDGET_H
#define PROJECTEVENTSWIDGET_H

#include <QWidget>
#include <QTreeView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStandardItem>
#include <QHeaderView>
#include <QCalendarWidget>

#include "calendar.h"

/*!
 * \brief Left Dock de la fenêtre d'Agenda. Contient le calendrier, le tree view des projets/taches/programamtions et la liste des évènements traditionnels.
 */
class ProjectEventsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ProjectEventsWidget(QWidget *parent = 0);

signals:
    void dateSelected(QDate);

    void projectDoubleClicked(Project&);
    void projectSelected(Project&);
    void taskSelected(Task&);

    void selectEvtToModif(Event&);


public slots:
    void slotDateSelected(QDate);
    void slotTreeViewClicked(QModelIndex);
    void slotTreeViewDoubleClicked(QModelIndex);

public:
    void updateTreeView();
    void setCalendarDate(QDate date);

private:

    void updateTasksTreeView();
    void updateEventsTreeView();

    QCalendarWidget *_calendar;
    QTreeView* _tasksTreeView;
    QTreeView* _eventsTreeView;
    QStandardItem* getTreeStandardItem(const Project& project);
    QStandardItem* getTreeStandardItem(const UnitTask& task);
    QStandardItem* getTreeStandardItem(const CompositeTask& task);
    QStandardItem* getTreeStandardItem(const Event& task);


};

#endif // PROJECTEVENTSWIDGET_H
