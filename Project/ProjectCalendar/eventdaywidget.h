#ifndef EVENTDAYWIDGET_H
#define EVENTDAYWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QPainter>
#include "calendar.h"

/*!
 * \brief Widget représentant un seul évènement dans l'agenda
 */
class EventWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EventWidget(const Event &event, QWidget *parent = 0);

signals:

public slots:

private:

    const Event& _event;
    void paintEvent(QPaintEvent* event);


};

/*!
 * \brief Widget représentant un seul jour dans l'agenda
 */
class EventDayWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EventDayWidget(const QDate& date, unsigned int scale, QWidget *parent = 0);

signals:

public slots:

private:

    void updateDay();

    const QDate& _date;
    unsigned int _scale; //in minutes
    QGridLayout *_gridLayout;

};

#endif // EVENTDAYWIDGET_H
