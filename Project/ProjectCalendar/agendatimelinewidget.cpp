#include "agendatimelinewidget.h"

AgendaTimelineWidget::AgendaTimelineWidget(unsigned int scale, QWidget *parent) : QWidget(parent), _scale(scale)
{

    this->setFixedHeight(1400);
    this->setFixedWidth(100);
    updateTimeline();
}

void AgendaTimelineWidget::updateTimeline()
{

    this->setMaximumHeight(60*24/_scale*50);


    for (unsigned int i = 0; i < 60*24/_scale; ++i)
    {
        QTime time = QTime(i*_scale/60, (i*_scale)%60);

        QString string = "<center>";
        string.append(time.toString("hh:mm"));
        string.append("</center>");

        QLabel *label = new QLabel(string, this);
        label->setFixedHeight(50);
        label->setGeometry(0, i*50-25+10, 50, 50);


    }



}

