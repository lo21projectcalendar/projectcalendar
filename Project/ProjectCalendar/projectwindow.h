#ifndef PROJECTWINDOW_H
#define PROJECTWINDOW_H

#include <QMainWindow>
#include <QDockWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeView>
#include <QHeaderView>

#include "calendar.h"

#include "projecttasksview.h"
#include "taskdependenciesgraphwidget.h"
#include "taskinspectorwidget.h"
#include "projectinspectorwidget.h"

/*!
 * \brief Fenêtre des Projets
 */
class ProjectWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit ProjectWindow(Project &project, QWidget *parent = 0);

signals:
    void closed();
public slots:

    void slotTaskDeleted(Task& task);
    void slotTaskSelected(Task* task);
    void slotTaskUpdated(Task& task);
    void slotTaskAdded(Task& task);

    void slotProjectSelected();
    void slotProjectUpdated(Project &project);
    void slotProjectDeleted(Project &project);


protected:
    void closeEvent(QCloseEvent *);

private:

    Project&            _project;

    QScrollArea*        _centerScrollArea;
    QDockWidget*        _projectDockWidget;
    QDockWidget*        _inspectorDockWidget;


    ProjectTasksView*            _projectTasksView;
    TaskDependenciesGraphWidget* _graphWidget;
    TaskInspectorWidget*         _taskInspectorWidget;
    ProjectInspectorWidget*      _projectInspectorWidget;

    void    setUpGraphCentralWidget();
    void    setUpGraphWidget();

    void    setUpProjectDockWidget();
    void    setUpProjectWidget();

    void    setUpInspectorDockWidget();
    void    setUpTaskInspectorWidget(Task *task);
    void    setUpProjectInspectorWidget();

};

#endif // PROJECTWINDOW_H
