#ifndef AGENDATIMELINEWIDGET_H
#define AGENDATIMELINEWIDGET_H

#include <QWidget>
#include <QTime>
#include <QLabel>

/*!
 * \brief Timeline dans l'Agenda
 */
class AgendaTimelineWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AgendaTimelineWidget(unsigned int scale, QWidget *parent = 0);

signals:

public slots:

private:

    void updateTimeline();

    unsigned int _scale;
};

#endif // AGENDATIMELINEWIDGET_H
