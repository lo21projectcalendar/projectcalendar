#ifndef TASKINSPECTORWIDGET_H
#define TASKINSPECTORWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QCheckBox>
#include <QTimeEdit>
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QDateEdit>
#include <QComboBox>
#include <QListWidget>
#include <QPushButton>
#include <QMenu>
#include <QMessageBox>
#include "calendar.h"

/*!
 * \brief Permet de visualiser et de modifier les information d'une tâche
 */
class TaskInspectorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TaskInspectorWidget(Task* task,QWidget *parent = 0);

    void setTask(Task* task);

signals:
    void taskUpdated(Task& task);
    void taskDeleted(Task& task);



public slots:
    void lineEditReturned();

    void preemptiveChecked(int);
    void durationTimeChanged(QTime);
    void availableDateChanged(QDate);
    void deadlineDateChanged(QDate);
    void fatherIndexChanged(int);
    void addPredecessor(QAction*);
    void removePredecessorChecked(bool);
    void deleteTask(bool);

private:
    Task* _task;


    QVBoxLayout     *_vLayout;

    QFormLayout     *_formLayout;


    QLineEdit       *_titleEdit;
    QTimeEdit       *_durationTimeEdit;
    QCheckBox       *_preemptiveCheckBox;

    QDateEdit       *_availableDateEdit;
    QDateEdit       *_deadlineDateEdit;


    QComboBox       *_fatherComboBox;

    QWidget         *_predecessorsWidget;
    QGridLayout     *_predecessorsGridLayout;
    QListWidget     *_predecessorsListWidget;
    QPushButton     *_addPredecessorButton;
    QPushButton     *_removePredecessorButton;

    QPushButton     *_deleteButton;

    void updateTask();
    void clear(QLayout *layout);

};

#endif // TASKINSPECTORWIDGET_H
