#ifndef PROJECTINSPECTORWIDGET_H
#define PROJECTINSPECTORWIDGET_H

#include <QWidget>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QDateEdit>
#include <QMessageBox>

#include "calendar.h"

/*!
 * \brief Permet de visualiser et de modifier les information d'un projet
 */
class ProjectInspectorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ProjectInspectorWidget(Project *project = 0, QWidget *parent = 0);

signals:
    void  projectUpdated(Project &project);
    void  projectDeleted(Project &project);


public slots:
    void  titleUpdated();
    void  deleteProject(bool);


private:

    void setup();

    Project*    _project;

    QLineEdit*  _titleLineEdit;
    QPushButton* _deleteButton;

};

#endif // PROJECTINSPECTORWIDGET_H
