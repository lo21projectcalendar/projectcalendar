#include "taskinspectorwidget.h"

TaskInspectorWidget::TaskInspectorWidget(Task* task,QWidget *parent) : QWidget(parent)
{
    this->setMinimumWidth(300);

    _vLayout = 0;


    setTask(task);



}

void TaskInspectorWidget::clear(QLayout *layout)
{

    if (layout)
    {
        QLayoutItem * item;
        QLayout * sublayout;
        QWidget * widget;
        while ((item = layout->takeAt(0))) {
            if ((sublayout = item->layout()) != 0) { clear(sublayout); }
            else if ((widget = item->widget()) != 0) {widget->hide(); delete widget;}
            else {delete item;}
        }

        delete layout;
    }

}

void TaskInspectorWidget::setTask(Task *task)
{




    if (!task)
       return;

    _task = task;
    //clear(_vLayout);


    bool enable = _task->canBeModified();



    _vLayout = new QVBoxLayout;
    _formLayout = new QFormLayout();
    _predecessorsGridLayout = new QGridLayout();





        //LINE EDIT
        _titleEdit = new QLineEdit(task->getTitle());
        QObject::connect(_titleEdit, SIGNAL(returnPressed()), this, SLOT(lineEditReturned()));
        QObject::connect(_titleEdit, SIGNAL(editingFinished()), this, SLOT(lineEditReturned()));

        _formLayout->addRow("Title : ", _titleEdit);

        if (UnitTask *unit = dynamic_cast<UnitTask*>(_task))
        {

            //CHECKBOX
            _preemptiveCheckBox = new QCheckBox();

            _preemptiveCheckBox->setChecked(unit->isPreemptive());
            _preemptiveCheckBox->setEnabled(enable);

            QObject::connect(_preemptiveCheckBox, SIGNAL(stateChanged(int)), this, SLOT(preemptiveChecked(int)) );
            _formLayout->addRow("Preemptive : ", _preemptiveCheckBox);

        }

        //DURATION
        Duree d = _task->getDuration();
        _durationTimeEdit = new QTimeEdit(QTime(d.getHeures(), d.getMinutes()));
        _durationTimeEdit->setEnabled(enable);
        QObject::connect(_durationTimeEdit, SIGNAL(timeChanged(QTime)), this, SLOT(durationTimeChanged(QTime)));
        _formLayout->addRow("Duration : ", _durationTimeEdit);

        _availableDateEdit = new QDateEdit(_task->getAvailable());
        _availableDateEdit->setEnabled(enable);

        QObject::connect(_availableDateEdit, SIGNAL(dateChanged(QDate)), this, SLOT(availableDateChanged(QDate)));
        _formLayout->addRow("Available Date : ", _availableDateEdit);

        _deadlineDateEdit = new QDateEdit(_task->getDeadline());
        _deadlineDateEdit->setEnabled(enable);

        QObject::connect(_deadlineDateEdit, SIGNAL(dateChanged(QDate)), this, SLOT(deadlineDateChanged(QDate)));
        _formLayout->addRow("Deadline : ", _deadlineDateEdit);


        //FATHER
        _fatherComboBox = new QComboBox();
        _fatherComboBox->setEnabled(enable);

        unsigned int currentIndex = 0;
        _fatherComboBox->addItem("- NO FATHER -", "");

        TaskManager& tm = TaskManager::getInstance();

        unsigned int i = 1;
        for (TaskManager::Iterator it = tm.getConstIteratorForProject(_task->getProject()); !it.isDone(); it.next())
        {
            CompositeTask *ct = dynamic_cast<CompositeTask*>(&it.currentItem());

            if (ct && ct != _task)
            {
                _fatherComboBox->addItem(ct->getTitle(), ct->getId());

                if (ct == _task->getFather())
                    currentIndex = i;

                ++i;

            }

        }

        _fatherComboBox->setCurrentIndex(currentIndex);

        //La connection se fait après le currentIndex car ce dernier appel le slot de MAJ
        QObject::connect(_fatherComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(fatherIndexChanged(int)));


        _formLayout->addRow("Father : ", _fatherComboBox);




        //PREDECESSORS
        _predecessorsWidget     = new QWidget();
        _predecessorsWidget->setEnabled(enable);

        _predecessorsListWidget = new QListWidget();


        for (TaskManager::Iterator it = tm.getConstPredecessorIteratorForTask(*_task); !it.isDone(); it.next())
        {
            QListWidgetItem *item = new QListWidgetItem(it.currentItem().getTitle());
            item->setData(QVariant::UserType, it.currentItem().getId());
            _predecessorsListWidget->addItem(item);


        }

        _predecessorsGridLayout->addWidget(_predecessorsListWidget, 0, 0, 1, 2);

        _addPredecessorButton = new QPushButton("+");

        QMenu *menu = new QMenu();

        for (TaskManager::Iterator it = tm.getConstPossiblePredecessorsIteratorForTask(*_task); !it.isDone(); it.next())
        {
            QAction *action = new QAction(it.currentItem().getTitle(), menu);
            action->setData(it.currentItem().getId());
            menu->addAction(action);

        }

        QObject::connect(menu, SIGNAL(triggered(QAction*)), this, SLOT(addPredecessor(QAction*)));
        _addPredecessorButton->setMenu(menu);


        _removePredecessorButton = new QPushButton("-");

        _predecessorsGridLayout->addWidget(_addPredecessorButton, 1, 0);
        _predecessorsGridLayout->addWidget(_removePredecessorButton, 1, 1);
        QObject::connect(_removePredecessorButton, SIGNAL(clicked(bool)), this, SLOT(removePredecessorChecked(bool)));


        _predecessorsWidget->setLayout(_predecessorsGridLayout);


        _formLayout->addRow("Predecessors : ", _predecessorsWidget);



        if (dynamic_cast<CompositeTask*>(_task))
        {
            _durationTimeEdit->setEnabled(false);
            _availableDateEdit->setEnabled(false);
            _deadlineDateEdit->setEnabled(false);
        }


        _vLayout->addLayout(_formLayout);


        _deleteButton = new QPushButton("Delete");
        _deleteButton->setEnabled(enable);

        QObject::connect(_deleteButton, SIGNAL(clicked(bool)), this, SLOT(deleteTask(bool)));

        _vLayout->addWidget(_deleteButton);





    setLayout(_vLayout);
}

void TaskInspectorWidget::lineEditReturned()
{
    _task->setTitle(_titleEdit->text());

    updateTask();
}

void TaskInspectorWidget::preemptiveChecked(int checked)
{


    UnitTask& ut = dynamic_cast<UnitTask&>(*_task);

    ut.setPreemptive(checked);
    updateTask();
}

void TaskInspectorWidget::durationTimeChanged(QTime duration)
{

    UnitTask& unitTask = dynamic_cast<UnitTask&>(*_task);

    try
    {
    unitTask.setDuration(duration);
    }
    catch(CalendarException e)
    {
        //_durationTimeEdit->setTime(QTime(unitTask.getDuration().getHeures(), unitTask.getDuration().getMinutes()));
          QMessageBox::information(this, "Error", e.getInfo());
    }
    updateTask();
}

void TaskInspectorWidget::availableDateChanged(QDate available)
{

    UnitTask& unitTask = dynamic_cast<UnitTask&>(*_task);

    try
    {
    unitTask.setAvailable(available);
    }
    catch(CalendarException e)
    {
          QMessageBox::information(this, "Error", e.getInfo());
          //_availableDateEdit->setDate(unitTask.getAvailable());

    }
    updateTask();

}

void TaskInspectorWidget::deadlineDateChanged(QDate deadline)
{

    UnitTask& unitTask = dynamic_cast<UnitTask&>(*_task);
    try
    {
    unitTask.setDeadline(deadline);
    }
    catch(CalendarException e)
    {

          QMessageBox::information(this, "Error", e.getInfo());
          //_deadlineDateEdit->setDate(unitTask.getDeadline());

    }
    updateTask();
}

void TaskInspectorWidget::fatherIndexChanged(int index)
{


    if (index == 0)
    {
        qDebug() << "NO FATHER";
        PrecedenceManager::getInstance().deletePrecedencesRelatedToTask(*_task);
        CompositeManager::getInstance().deleteCompositionRelatedToTask(*_task);

    }
    else
    {
        QString taskID = _fatherComboBox->itemData(index).toString();
        qDebug() << "Task ID : " << taskID;

        CompositeTask& newFather = dynamic_cast<CompositeTask&>(TaskManager::getInstance().getTask(taskID, _task->getProject()));

        //On devrait ajouter au(x) fils les précédences du père
        PrecedenceManager::getInstance().deletePrecedencesRelatedToTask(*_task);
        CompositeManager::getInstance().deleteCompositionRelatedToTask(*_task);

        CompositeManager::getInstance().addComposition(newFather, *_task);

    }


    updateTask();

}

void TaskInspectorWidget::addPredecessor(QAction * action)
{
    QString taskID = action->data().toString();

    Task &predecessor = TaskManager::getInstance().getTask(taskID, _task->getProject());

    PrecedenceManager::getInstance().addPrecedence(predecessor, *_task);



    updateTask();

}

void TaskInspectorWidget::removePredecessorChecked(bool)
{
    if ( _predecessorsListWidget->selectedItems().length())
    {
        QListWidgetItem *selectedItem = _predecessorsListWidget->selectedItems()[0];
        QString taskID = selectedItem->data(QVariant::UserType).toString();

        Task &predecessor = TaskManager::getInstance().getTask(taskID, _task->getProject());

        PrecedenceManager::getInstance().deletePrecedence(predecessor, *_task);

    }

    updateTask();


}


void TaskInspectorWidget::deleteTask(bool)
{
     emit taskDeleted(*_task);
}

void TaskInspectorWidget::updateTask()
{
    emit taskUpdated(*_task);
}
