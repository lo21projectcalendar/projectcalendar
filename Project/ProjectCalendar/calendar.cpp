//
//  calendar.cpp
//  ProjectCalendar
//
//  Created by Aladin TALEB on 09/04/2015.
//  Copyright (c) 2015 Aladin TALEB. All rights reserved.
//

#include "calendar.h"

DataManager::SingletonHandler DataManager::handler;


//******************************************************************************************
/*Project*/

bool Project::isFullyScheduled() const
{
    bool isScheduled = true;

    for (TaskManager::ConstIterator it = TaskManager::getInstance().getConstRootIteratorForProjectAndRootTask(*this, 0); isScheduled && !it.isDone(); it.next())
    {
        isScheduled = it.currentItem().isFullyScheduled();
    }

    return isScheduled;
}

void Project::setName(const QString& name)
{

    if (name == _name)
        return;

    if (ProjectManager::getInstance().containsProject(name))
        throw CalendarException("This project already exists");
    _name = name;
}

QDate Project::getAvailable() const
{

    TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(*this, 0);

    if (it.isDone())
        return QDate();



    QDate available = it.currentItem().getAvailable();

    it.next();

    for (; !it.isDone(); it.next())
    {
        QDate a = it.currentItem().getAvailable();

        if (available < a)
            available = a;
    }

    return available;
}

QDate Project::getDeadline() const
{
    TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(*this, 0);

    if (it.isDone())
        return QDate();



    QDate deadline = it.currentItem().getDeadline();

    it.next();

    for (; !it.isDone(); it.next())
    {
        QDate d = it.currentItem().getDeadline();

        if (deadline > d)
            deadline = d;
    }

    return deadline;
}


//******************************************************************************************
/*Task*/
#pragma mark - Task

unsigned int   Task::getPrecedenceDepth() const
{

    TaskManager &tm = TaskManager::getInstance();

    unsigned int depth = 0;

    for (TaskManager::Iterator it = tm.getPredecessorIteratorForTask(*this); !it.isDone(); it.next())
    {
        unsigned int predDepth = it.currentItem().getPrecedenceDepth()+1;
        if (predDepth > depth)
            depth = predDepth;
    }

    return depth;


}



bool Task::canBeScheduled() const
{

    //Pas de programmation si la tache est déjà programmée
    if (isFullyScheduled())
        return false;

    //Les contraintes de précédences du père sont héritées par le fils
    CompositeTask *father = getFather();
    if (father && !father->canBeScheduled())
        return false;


    bool canBeScheduled = true;

    for (TaskManager::ConstIterator it = TaskManager::getInstance().getConstPredecessorIteratorForTask(*this); canBeScheduled && !it.isDone();  it.next())
    {
        canBeScheduled = it.currentItem().isFullyScheduled();
    }


    return canBeScheduled;

}

void Task::setId(const QString &id)
{
    TaskManager& tm = TaskManager::getInstance();
    
    if (tm.containsTask(id, _project))
    {
        throw CalendarException("This ID already exists");
    }
    
    _id = id;
    
}


CompositeTask*     Task::getFather() const
{
    return CompositeManager::getInstance().getFatherOf(*this);
}

//******************************************************************************************
/*Composite Task*/
#pragma mark - CompositeTask




QDate CompositeTask::getAvailable() const
{

    TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(_project, this);

    if (it.isDone())
        return QDate();



    QDate available = it.currentItem().getAvailable();

    it.next();

    for (; !it.isDone(); it.next())
    {
        QDate a = it.currentItem().getAvailable();

        if (available < a)
            available = a;
    }

    return available;
}

QDate CompositeTask::getDeadline() const
{
    TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(_project, this);

    if (it.isDone())
        return QDate();



    QDate deadline = it.currentItem().getDeadline();

    it.next();

    for (; !it.isDone(); it.next())
    {
        QDate d = it.currentItem().getDeadline();

        if (deadline > d)
            deadline = d;
    }

    return deadline;
}

Duree CompositeTask::getDuration() const
{
    Duree d = Duree();

    for (TaskManager::Iterator it = TaskManager::getInstance().getRootIteratorForProjectAndRootTask(_project, this); !it.isDone(); it.next())
    {
        d=d+it.currentItem().getDuration();
    }

    return d;
}


bool CompositeTask::isFullyScheduled() const
{
    bool isScheduled = true;

    for (TaskManager::ConstIterator it = TaskManager::getInstance().getConstRootIteratorForProjectAndRootTask(getProject(),this); isScheduled && !it.isDone();  it.next())
    {
        isScheduled = it.currentItem().isFullyScheduled();
    }

    return isScheduled;
}

bool CompositeTask::canBeModified() const
{

    bool containsTasks = false;

    for (TaskManager::ConstIterator it = TaskManager::getInstance().getConstRootIteratorForProjectAndRootTask(getProject(), this); !containsTasks && !it.isDone(); )
        containsTasks = true;

    return !containsTasks || !isPartiallyScheduled();

}


bool CompositeTask::isPartiallyScheduled() const
{
    bool isScheduled = true;

    for (TaskManager::ConstIterator it = TaskManager::getInstance().getConstRootIteratorForProjectAndRootTask(getProject(),this); isScheduled && !it.isDone();  it.next())
    {
        isScheduled = it.currentItem().isPartiallyScheduled();
    }

    return isScheduled;
}
//******************************************************************************************
/*Unit Task*/

QDate       UnitTask::getAvailable() const {return _available;}
QDate       UnitTask::getDeadline() const {return _deadline;}
Duree       UnitTask::getDuration() const {return _duration;}

bool UnitTask::isFullyScheduled() const
{
    return getScheduledTimeLeft().getDureeEnMinutes() == 0;

}

bool UnitTask::isPartiallyScheduled() const
{
    return getScheduledTimeLeft().getDureeEnMinutes() != _duration.getDureeEnMinutes();
}

bool UnitTask::canBeModified() const
{

    return !isPartiallyScheduled();

}

Duree   UnitTask::getScheduledTimeDone() const
{
    Duree duree(0,0);

    for (EventManager::ConstIterator it = EventManager::getInstance().getEventTaskRelatedConstIterator(*this); !it.isDone(); it.next())
    {
        const EventTask& event = dynamic_cast<const EventTask&>(it.currentItem());
        duree = duree + event.getDuration();
    }

    return duree;
}

Duree   UnitTask::getScheduledTimeLeft() const
{
    int nbMinutes = getDuration().getDureeEnMinutes() - getScheduledTimeDone().getDureeEnMinutes();
    return Duree(nbMinutes/60, nbMinutes%60);
}


//******************************************************************************************
/*Project Manager*/
#pragma mark - ProjectManager

ProjectManager& ProjectManager::getInstance() {return DataManager::getProjectManager();}

Project& ProjectManager::getProject(const QString &name)
{
    if (Project* p = findObject(ProjectNameStrategy(name)))
        return *p;

    throw CalendarException("This project doesn't exist");
}



Project& ProjectManager::addProject(const QString& name)
{


    if (findObject(ProjectNameStrategy(name)))
        throw CalendarException("This project already exists");


    Project *newProject = new Project(name);

    addObject(*newProject);

    return *newProject;
}

bool ProjectManager::containsProject(const QString& name)
{
    return findObject(ProjectNameStrategy(name));
}

void      ProjectManager::deleteProject(const Project& project)
{

    deleteObjects(ProjectNameStrategy(project.getName()), DeleteElementsRoutineStrategy());
}

void ProjectManager::DeleteElementsRoutineStrategy::operator ()(const Project& project) const
{
    TaskManager::getInstance().deleteTasks(project);
}

//******************************************************************************************
/*Task Manager*/
#pragma mark - TaskManager

TaskManager& TaskManager::getInstance() {return DataManager::getTaskManager();}

UnitTask& TaskManager::addUnitTask(Project &project, const QString& id, const QString& title, const Duree& duree, const QDate& available, const QDate& deadline, bool preemptive)
{


    if (findTask(id, project))
        throw CalendarException("This task already exists");

    UnitTask *unitTask = new UnitTask(project, id, title, duree, available, deadline, preemptive);



    addObject(*unitTask);

    return *unitTask;

}

CompositeTask& TaskManager::addCompositeTask(Project &project, const QString& id, const QString& title)
{

    if (findTask(id, project))
        throw CalendarException("This task already exists");

    CompositeTask *compositeTask = new CompositeTask(project, id, title);
    addObject(*compositeTask);

    return *compositeTask;
}

void TaskManager::deleteTask(Task &task, bool fromFather)
{

    PrecedenceManager::getInstance().deletePrecedencesRelatedToTask(task);


    if (!fromFather)
      CompositeManager::getInstance().deleteCompositionRelatedToTask(task);


    if (CompositeTask* ct = dynamic_cast<CompositeTask*>(&task))
    {
        CompositeManager::getInstance().deleteCompositionAndSubtasks(*ct);

    }
    else if (UnitTask* ut = dynamic_cast<UnitTask*>(&task))
    {
        EventManager::getInstance().deleteEvents(*ut);
    }


    deleteObject(task);
}

void TaskManager::DeleteTasksRoutineStrategy::operator ()(const Task& task) const
{
    PrecedenceManager::getInstance().deletePrecedencesRelatedToTask(task);


    CompositeManager::getInstance().deleteCompositionRelatedToTask(task);


    if (const CompositeTask* ct = dynamic_cast<const CompositeTask*>(&task))
    {
        CompositeManager::getInstance().deleteCompositionAndSubtasks(*ct);

    }
    else if (const UnitTask* ut = dynamic_cast<const UnitTask*>(&task))
    {
        EventManager::getInstance().deleteEvents(*ut);
    }

}

Task&   TaskManager::getTask(const QString &id, const Project& project) const
{
    Task *t = findTask(id, project);

    if (!t)
        throw CalendarException("Task doesn't exist with this ID");

    return *t;
}

bool TaskManager::containsTask(const QString& id, const Project& project)
{
    return findTask(id, project) != 0;
}

Task* TaskManager::findTask(const QString &id, const Project &project) const
{

   return findObject(IDIteratorStrategy(project, id));

}

QString TaskManager::generateID(const Project &project) const
{
    QString str = "";
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    const int stringLength = 12; // assuming you want random strings of 12 characters


    do
    {
        str = project.getName();
        str.append('_');
        for (int i = 0; i < stringLength; ++i)
        {
            int randomIndex = qrand()%possibleCharacters.length();
            QChar randomChar = possibleCharacters.at(randomIndex);
            str.append(randomChar);
        }

    } while (findTask(str, project));

    return str;

}

bool TaskManager::ProjectIteratorStrategy::operator()(const Task& task) const
{

    return (&(task.getProject()) == &_project);
}

bool TaskManager::RootIteratorStrategy::operator()(const Task& task) const
{
    if (!ProjectIteratorStrategy::operator()(task))
        return false;

    return (_rootTask == task.getFather());
}

bool TaskManager::SchedulableTaskIteratorStrategy::operator()(const Task& task) const
{
    if (!ProjectIteratorStrategy::operator()(task))
        return false;

    return task.canBeScheduled();
}

bool TaskManager::PredecessorIteratorStrategy::operator()(const Task& task) const
{
    if (!ProjectIteratorStrategy::operator ()(task))
        return false;

    return PrecedenceManager::getInstance().containsPrecedence(task, _next);
}

bool TaskManager::PossiblePredecessorsIteratorStrategy::operator()(const Task& task) const
{
    if (!ProjectIteratorStrategy::operator ()(task) || _next.getFather() != task.getFather())
        return false;

    try
    {
        PrecedenceManager::getInstance().addPrecedence(task, _next);
    }
    catch (...)
    {
        return false;
    }

    PrecedenceManager::getInstance().deletePrecedence(task, _next);

    return true;
}

bool TaskManager::SuccessorIteratorStrategy::operator()(const Task& task) const
{
    if (!ProjectIteratorStrategy::operator ()(task))
        return false;

    return PrecedenceManager::getInstance().containsPrecedence(_previous, task);
}



//******************************************************************************************
/*Precedence Manager*/
#pragma mark - PrecedenceManager


PrecedenceManager& PrecedenceManager::getInstance()
{return DataManager::getPrecedenceManager();}


void PrecedenceManager::deletePrecedence(const Task &previous, const Task &next)
{
    PrecedenceSelectionStrategy *strategy = new PrecedenceSelectionStrategy(previous, next);
    deleteObjects(*strategy);

}

void  PrecedenceManager::deletePrecedencesRelatedToTask(const Task& task)
{
    TaskLinkDeleteSelectionStrategy *strategy = new TaskLinkDeleteSelectionStrategy(task);
    deleteObjects(*strategy);
}

bool PrecedenceManager::containsPrecedence(const Task& previous, const Task& next) const
{
    return findObject(PrecedenceSelectionStrategy(previous, next));
}

bool PrecedenceManager::containsRecursivePrecedence(const Task &previous, const Task &next, bool checkEqual) const
{
    if (checkEqual && &previous == &next)
        return true;

    if (containsPrecedence(previous, next))
        return true;

    for (TaskManager::Iterator it = TaskManager::getInstance().getConstPredecessorIteratorForTask(next); !it.isDone(); it.next())
    {
        if (containsRecursivePrecedence(previous, it.currentItem(), checkEqual))
            return true;
    }

    return false;


}

bool PrecedenceManager::containsCycle(const Task& task) const
{
    return containsRecursivePrecedence(task, task, false);
}

void  PrecedenceManager::addPrecedence(const Task& previous, const Task& next)
{
    if (containsRecursivePrecedence(previous, next))
        throw CalendarException("Recursive Precedence Link");

    Precedence *precedence = new Precedence(previous, next);
    addObject(*precedence);

    if (containsCycle(next))
    {
        deletePrecedence(previous, next);
        throw CalendarException("Circular Precedence Link");
    }

}

bool PrecedenceManager::TaskLinkDeleteSelectionStrategy::operator() (const Precedence& precedence) const
{
    return &precedence.getNext() == &_task || &precedence.getPrevious() == &_task;
}


//******************************************************************************************
/*Composite Manager*/
#pragma mark - CompositeManager

CompositeManager& CompositeManager::getInstance() {return DataManager::getCompositeManager();}


CompositeTask*  CompositeManager::getFatherOf(const Task& son) const
{

    Composite* composite = findObject(SonToFatherSelectionStrategy(son));
    if (!composite)
        return 0;


    return &composite->getFather();
}

bool CompositeManager::containsComposition(const CompositeTask& father, const Task& son) const
{
    return findObject(SonFatherSelectionStrategy(son, father));
}

bool CompositeManager::containsRecursiveComposition(const CompositeTask& ancestor, const Task& son) const
{
    if (&ancestor == &son)
        return false;

    if (containsComposition(ancestor, son))
        return true;


    Task* father = getFatherOf(son);

    if (!father)
        return false;

    return containsRecursiveComposition(ancestor, *father);

}

void  CompositeManager::addComposition(CompositeTask& father, Task& son)
{
    if (&father == &son)
        throw CalendarException("Self composition");

    if (containsRecursiveComposition(father, son))
        throw CalendarException("Circular Composition");
    
    Composite *composite = new Composite(father, son);
    addObject(*composite);
}

void  CompositeManager::deleteCompositionRelatedToTask(const Task& task)
{
    SonToFatherSelectionStrategy *selectionStrategy = new SonToFatherSelectionStrategy(task);
    deleteObjects(*selectionStrategy);
}

void CompositeManager::deleteCompositionAndSubtasks(const CompositeTask &task){

    FatherToSonSelectionStrategy *selectionStrategy = new FatherToSonSelectionStrategy(task);
    RecursiveDeleteRoutineStrategy *routineStrategy = new RecursiveDeleteRoutineStrategy(task);

    deleteObjects(*selectionStrategy, *routineStrategy);

}



void CompositeManager::RecursiveDeleteRoutineStrategy::operator ()(const Composite& composite) const
{
    if (&composite.getFather() == &_task)
    {
        TaskManager::getInstance().deleteTask(composite.getSon(), true);
    }

}

//******************************************************************************************
/*Data Manager*/
#pragma mark - DataManager



 ProjectManager& DataManager::getProjectManager()
 {return DataManager::handler._projectManager;}
 TaskManager& DataManager::getTaskManager()
 {return DataManager::handler._taskManager;}
 CompositeManager& DataManager::getCompositeManager() {return DataManager::handler._compositeManager;}
 PrecedenceManager& DataManager::getPrecedenceManager() {return DataManager::handler._precedenceManager;}
 EventManager& DataManager::getEventManager() {return DataManager::handler._eventManager;}

void DataManager::freeHandler()
{
    DataManager::handler.~SingletonHandler();
}


//******************************************************************************************
/*Data Post*/

bool DataManager::DataPostDriveFileStrategy::operator ()(QByteArray byteArray)
{
    QFileDialog *fd = new QFileDialog;
    fd->setFileMode(QFileDialog::AnyFile);
    fd->setAcceptMode(QFileDialog::AcceptSave);
    QString path;

    if (fd->exec()) {
        // save data to a file
        path = fd->selectedFiles()[0];
    }

    delete fd;

    if (path.length() == 0)
        return false;

    if (QFile::exists(path))
    {
        QFile::remove(path);
    }




    QFile file(path);
    if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate))
        return false;

    file.write(byteArray);
    file.close();

    return true;

}

//******************************************************************************************
/*Data Save*/


QByteArray DataManager::DataSaveXMLStrategy::operator ()()
{

    QByteArray byteArray;


    QXmlStreamWriter out(&byteArray);
    out.setAutoFormatting(true);
    out.writeStartDocument();

    ProjectManager &prom = ProjectManager::getInstance();
    TaskManager &tm = TaskManager::getInstance();
    CompositeManager &cm = CompositeManager::getInstance();
    PrecedenceManager &prem = PrecedenceManager::getInstance();
    EventManager &em = EventManager::getInstance();

    out.writeStartElement("DATA");


    out.writeStartElement("PROJECTS");

    for (ProjectManager::ConstIterator proIT = prom.constIterator(); !proIT.isDone(); proIT.next())
    {

       out.writeStartElement("PROJECT");

       out.writeTextElement("NAME", proIT.currentItem().getName());

        out.writeStartElement("TASKS");

        for (TaskManager::ConstIterator tIT = tm.getConstIteratorForProject(proIT.currentItem()); !tIT.isDone(); tIT.next())
        {
            if (dynamic_cast<const UnitTask*>(&tIT.currentItem()))
                out.writeStartElement("UNIT-TASK");
            else if (dynamic_cast<const CompositeTask*>(&tIT.currentItem()))
                out.writeStartElement("COMPOSITE-TASK");

            out.writeTextElement("ID", tIT.currentItem().getId());
            out.writeTextElement("TITLE", tIT.currentItem().getTitle());

            if (const UnitTask *u = dynamic_cast<const UnitTask*>(&tIT.currentItem()))
            {

                out.writeTextElement("DURATION", QString::number(u->getDuration().getDureeEnMinutes()));
                out.writeTextElement("AVAILABLE", u->getAvailable().toString());
                out.writeTextElement("DEADLINE", u->getDeadline().toString());
                out.writeTextElement("PREEMPTIVE", u->isPreemptive()?"TRUE":"FALSE");


                out.writeStartElement("EVENTS");

                for (EventManager::ConstIterator it = em.getEventTaskRelatedConstIterator(*u); !it.isDone(); it.next())
                {
                    out.writeStartElement("EVENT");

                    out.writeTextElement("NAME", it.currentItem().getName());
                    out.writeTextElement("START-DATE", it.currentItem().getDateDebut().toString());
                    out.writeTextElement("END-DATE", it.currentItem().getDateFin().toString());

                    out.writeEndElement(); //EVENT

                }

                out.writeEndElement(); //EVENTS


            }


            out.writeEndElement(); //TASK

        }
        out.writeEndElement(); //TASKS

        out.writeStartElement("COMPOSITIONS");
        for (CompositeManager::ConstIterator cIT = cm.constIterator(); !cIT.isDone(); cIT.next())
        {
            if (&cIT.currentItem().getFather().getProject() == &proIT.currentItem())
            {
                out.writeStartElement("COMPOSITION");

                out.writeTextElement("FATHER", cIT.currentItem().getFather().getId());
                out.writeTextElement("SON", cIT.currentItem().getSon().getId());

                out.writeEndElement(); //COMPOSITION
            }

        }

        out.writeEndElement(); //COMPOSITIONS

        out.writeStartElement("PRECEDENCES");

        for (PrecedenceManager::ConstIterator preIT = prem.constIterator(); !preIT.isDone(); preIT.next())
        {
            if (&preIT.currentItem().getPrevious().getProject() == &proIT.currentItem())
            {
                out.writeStartElement("PRECEDENCE");

                out.writeTextElement("PREVIOUS", preIT.currentItem().getPrevious().getId());
                out.writeTextElement("NEXT", preIT.currentItem().getNext().getId());

                out.writeEndElement(); //PRECEDENCE
            }
        }

       out.writeEndElement(); //PRECEDENCES

       out.writeEndElement(); //PROJECT

    }
    out.writeEndElement(); //PROJECTS

    out.writeStartElement("EVENTS");

    for (EventManager::ConstIterator it = em.getEventTraditionnalConstIterator(); !it.isDone(); it.next())
    {

        const EventTraditionnal& et = dynamic_cast<const EventTraditionnal&>(it.currentItem());

        out.writeStartElement("EVENT");

        out.writeTextElement("NAME",et.getName());
        out.writeTextElement("START-DATE", et.getDateDebut().toString());
        out.writeTextElement("END-DATE", et.getDateFin().toString());
        out.writeTextElement("DESCRIPTION",et.getDescription());
        out.writeTextElement("PEOPLE",QString::number(et.getNbpersonne()));

        out.writeEndElement(); //EVENT

    }

    out.writeEndElement(); //EVENTS

    out.writeEndElement(); //DATA

    out.writeEndDocument();


    return byteArray;
}


//******************************************************************************************
/*Data Get*/

QByteArray DataManager::DataGetDriveFileStrategy::operator ()()
{

    QFileDialog *fd = new QFileDialog;
    fd->setFileMode(QFileDialog::AnyFile);
    fd->setAcceptMode(QFileDialog::AcceptOpen);
    fd->setNameFilter("XML (*.xml)");
    QString path;

    if (fd->exec())
    {
        path = fd->selectedFiles()[0];
    }

    qDebug() << path;

    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
        return 0;

    QString string = file.readAll();

    file.close();

    return string.toUtf8();
}

//******************************************************************************************
/*DATA LOAD*/

void DataManager::DataLoadXMLStrategy::operator ()(QByteArray byteArray)
{

    _reader = new QXmlStreamReader(byteArray);
    _reader->readNextStartElement();

    qDebug() << _reader->name();
   if (_reader->name() == "DATA")
               readData();


}

void DataManager::DataLoadXMLStrategy::readData()
{
    while (_reader->readNextStartElement()) {
           if (_reader->name() == "PROJECTS")
               readProjects();
           else if (_reader->name() == "EVENTS")
               readEventsTraditional();
           else
               _reader->skipCurrentElement();
       }
}

void DataManager::DataLoadXMLStrategy::readProjects()
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "PROJECT")
            readProject();
        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readProject()
{

    Project *project;

    while (_reader->readNextStartElement())
    {

        if (_reader->name() == "NAME")
        {
            QString projectName = _reader->readElementText();
            project = &ProjectManager::getInstance().addProject(projectName);

        }

        else if (_reader->name() == "TASKS")
            readTasks(*project);
        else if (_reader->name() == "COMPOSITIONS")
            readCompositions(*project);

        else if (_reader->name() == "PRECEDENCES")
            readPrecedences(*project);

        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readTasks(Project &project)
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "UNIT-TASK")
            readUnitTask(project);
        else if (_reader->name() == "COMPOSITE-TASK")
            readCompositeTask(project);

        else
            _reader->skipCurrentElement();
    }
}



void DataManager::DataLoadXMLStrategy::readPrecedences(Project &project)
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "PRECEDENCE")
            readPrecedence(project);

        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readCompositions(Project &project)
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "COMPOSITION")
            readComposition(project);

        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readUnitTask(Project &project)
{

    UnitTask *task;
    QString id;
    QString title;
    QDate available;
    QDate deadline;
    Duree duration;
    bool preemptive = false;


    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "ID")
        {
            id = _reader->readElementText();
        }
        else if (_reader->name() == "TITLE")
        {
            title = _reader->readElementText();
        }

        else if (_reader->name() == "AVAILABLE")
        {
            QString availableString = _reader->readElementText();
            available = QDate::fromString(availableString);
        }
        else if (_reader->name() == "DEADLINE")
        {
            QString deadlineString = _reader->readElementText();
            deadline =  QDate::fromString(deadlineString);
        }


        else if (_reader->name() == "DURATION")
        {
            QString durationString = _reader->readElementText();
            int durationValue = durationString.toInt();

            duration = Duree(durationValue/60, durationValue%60);
        }

        else if (_reader->name() == "PREEMPTIVE")
        {
            QString preemptiveString = _reader->readElementText();
            if (preemptiveString == "TRUE")
                preemptive = true;
        }

        else if (_reader->name() == "EVENTS")
        {
            task = &TaskManager::getInstance().addUnitTask(project, id, title, duration, available, deadline, preemptive);
            readEventsTask(*task);

        }
        else
            _reader->skipCurrentElement();
    }


}




void DataManager::DataLoadXMLStrategy::readEventsTask(const UnitTask &task)
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "EVENT")
            readEventTask(task);

        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readEventTask(const UnitTask &task)
{

    QString name;
    QDateTime startDate;
    QDateTime endDate;


    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "NAME")
            name = _reader->readElementText();

        else if (_reader->name() == "START-DATE")
            startDate =  QDateTime::fromString(_reader->readElementText());

        else if (_reader->name() == "END-DATE")
            endDate = QDateTime::fromString(_reader->readElementText());

        else
            _reader->skipCurrentElement();
    }

    EventManager::getInstance().addEventTask(name, startDate, endDate, task);
}


void DataManager::DataLoadXMLStrategy::readCompositeTask(Project &project)
{

    QString id;
    QString title;

    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "ID")
        {
            id = _reader->readElementText();

        }
        else if (_reader->name() == "TITLE")
        {
            title = _reader->readElementText();
        }
        else
            _reader->skipCurrentElement();
    }

    TaskManager::getInstance().addCompositeTask(project, id, title);
}

void DataManager::DataLoadXMLStrategy::readComposition(Project &project)
{

    CompositeTask *father = 0;
    Task *son = 0;

    while (_reader->readNextStartElement())
    {

        if (!father || !son)
        {
            if (_reader->name() == "FATHER")
            {
                QString taskID = _reader->readElementText();
                father = dynamic_cast<CompositeTask*>(&TaskManager::getInstance().getTask(taskID, project));

            }
            else if (_reader->name() == "SON")
            {
                QString taskID = _reader->readElementText();
                son = &TaskManager::getInstance().getTask(taskID, project);
            }
            else
                _reader->skipCurrentElement();


        }

    }

    if (father && son)
    {
        CompositeManager::getInstance().addComposition(*father, *son);
    }

}

void DataManager::DataLoadXMLStrategy::readPrecedence(Project &project)
{

    Task *previous = 0;
    Task *next = 0;

    while (_reader->readNextStartElement())
    {


        if (!previous || !next)
        {
            if (_reader->name() == "PREVIOUS")
            {
                QString taskID = _reader->readElementText();
                previous = &TaskManager::getInstance().getTask(taskID, project);

            }
            else if (_reader->name() == "NEXT")
            {
                QString taskID = _reader->readElementText();
                next = &TaskManager::getInstance().getTask(taskID, project);
            }
            else
                _reader->skipCurrentElement();


        }

    }

    if (previous && next)
    {
        PrecedenceManager::getInstance().addPrecedence(*previous, *next);
    }

}


void DataManager::DataLoadXMLStrategy::readEventsTraditional()
{
    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "EVENT")
            readEventTraditional();

        else
            _reader->skipCurrentElement();
    }
}

void DataManager::DataLoadXMLStrategy::readEventTraditional()
{

    QString name;
    QDateTime startDate;
    QDateTime endDate;
    QString description;
    int nbPeople;


    while (_reader->readNextStartElement())
    {
        if (_reader->name() == "NAME")
            name = _reader->readElementText();

        else if (_reader->name() == "START-DATE")
            startDate = QDateTime::fromString(_reader->readElementText());

        else if (_reader->name() == "END-DATE")
            endDate = QDateTime::fromString(_reader->readElementText());

        else if (_reader->name() == "DESCRIPTION")
            description = _reader->readElementText();

        else if (_reader->name() == "PEOPLE")
             nbPeople = _reader->readElementText().toInt();
        else
            _reader->skipCurrentElement();
    }

    EventManager::getInstance().addEventTrad(name, startDate, endDate, nbPeople, description);
}


/*************************************************************************/
//EventManager



EventManager& EventManager::getInstance()
{return DataManager::getEventManager();}


Event& EventManager::getEvent(const QString &name)
{
    if (Event* e = findObject(EventNameStrategy(name)))
        return *e;

    throw CalendarException("This event doesn't exist");
}


bool EventManager::isValid(const QDateTime &dateDebut, const QDateTime &dateFin, int nbPersonne){
    bool isOk=false;

        /** Test du nombre de personne **/
        if(nbPersonne<0)
            throw("Nombre de personne < 0");

        /** Test date **/
        if(dateDebut>dateFin || dateDebut==dateFin)
            throw(CalendarException("Date debut et date Fin incompatible"));

        /** Test par rapport à tous les event deja enregistre dans le Manager **/
        for(EventManager::Iterator it=EventManager::getInstance().iterator();!it.isDone(); it.next()) //tant que l'on a pas fait tous les event
        {
            if((dateDebut <= it.currentItem().getDateDebut() && !(dateFin<= it.currentItem().getDateDebut()))
                    || (dateDebut>it.currentItem().getDateDebut() && !(dateDebut>=it.currentItem().getDateFin())))
                throw(CalendarException("Incompatibilité date"));
        }
    isOk=true;
    return isOk;
}

bool EventManager::isValid(const QDateTime &dateDebut, const QDateTime &dateFin, UnitTask* t){
    bool isOk=false;

        if (!t->canBeScheduled())
            throw(CalendarException("Ne peut pas être programmée (vérifier contraintes de précédences/compositions/..."));

        /** Verification des dates **/
        if(dateDebut>dateFin || dateDebut==dateFin)
            throw(CalendarException("Date debut < ou = date Fin"));

        if(t->getAvailable()>dateDebut.date() || dateFin.date()>t->getDeadline())
            throw(CalendarException("Date Event et date Task incompatible"));


        /** Vérification des duree de l'event **/
        TIME::Duree dureeE=0;
        int i1=dateDebut.toTime_t() ;
        int i2=dateFin.toTime_t();
        int heure=(i2-i1)/3600;
        int min=((i2-i1)%3600)/60;
        dureeE.setDuree(heure,min);

        qDebug()<<"heure: "<<heure;
        qDebug()<<"min: "<<min;

        if(dureeE.getDureeEnMinutes() > t->getScheduledTimeLeft().getDureeEnMinutes())
            throw(CalendarException("Duree Event > Duree tache"));

        if(!(t->isPreemptive()))
            if(dureeE.getDureeEnHeures()>12)
                throw(CalendarException("Event task impossible: duree > 12h"));


        /** Test par rapport à tous les event deja enregistre dans le Manager **/
        for(EventManager::Iterator it=EventManager::getInstance().iterator();!it.isDone(); it.next()) //tant que l'on a pas fait tous les event
        {
            if((dateDebut <= it.currentItem().getDateDebut() && !(dateFin<= it.currentItem().getDateDebut()))
                    || (dateDebut>it.currentItem().getDateDebut() && !(dateDebut>=it.currentItem().getDateFin())))
                throw(CalendarException("Incompatibilité date"));
        }

    isOk=true;
    return isOk;
}

bool EventManager::isModifValid(const QDateTime &dateDebut, const QDateTime &dateFin, int nbPersonne,const QDateTime& oldDateDebut, const QDateTime& oldDateFin){
    bool isOk=false;
        /** Test du nombre de personne **/
        if(nbPersonne<0)
            throw("Nombre de personne < 0");

        /** Test date **/
        if(dateDebut>dateFin || dateDebut==dateFin)
            throw(CalendarException("Date debut et date Fin incompatible"));

        /** Test par rapport à tous les event deja enregistre dans le Manager **/
        for(EventManager::Iterator it=EventManager::getInstance().iterator();!it.isDone(); it.next()) //tant que l'on a pas fait tous les event
        {
            if((dateDebut <= it.currentItem().getDateDebut() && !(dateFin<= it.currentItem().getDateDebut()))
                    || (dateFin>it.currentItem().getDateFin() && !(dateDebut>=it.currentItem().getDateFin())))
                if(oldDateDebut!=it.currentItem().getDateDebut()&&oldDateFin!=it.currentItem().getDateFin())
                    throw(CalendarException("Incompatibilité date"));
        }
    isOk=true;

    /** Recherche de l'ancien evt pour le suprimer **/
    for(EventManager::Iterator it=EventManager::getInstance().iterator();!it.isDone(); it.next()) //tant que l'on a pas fait tous les event
    {
            if(oldDateDebut==it.currentItem().getDateDebut()&&oldDateFin==it.currentItem().getDateFin())
                throw(CalendarException("Event suprimé"));
    }

    return isOk;
}


bool EventManager::isModifValid(const QDateTime &dateDebut, const QDateTime &dateFin, UnitTask* t, const EventTask&){
    bool isOk=false;

        if (!t->canBeScheduled())
            throw(CalendarException("Ne peut pas être programmée (vérifier contraintes de précédences/compositions/..."));

        /** Verification des dates **/
        if(dateDebut>dateFin || dateDebut==dateFin)
            throw(CalendarException("Date debut < ou = date Fin"));

        if(t->getAvailable()>dateDebut.date() || dateFin.date()>t->getDeadline())
            throw(CalendarException("Date Event et date Task incompatible"));


        /** Vérification des duree de l'event **/
        TIME::Duree dureeE=0;
        int i1=dateDebut.toTime_t() ;
        int i2=dateFin.toTime_t();
        int heure=(i2-i1)/3600;
        int min=((i2-i1)%3600)/60;
        dureeE.setDuree(heure,min);

        qDebug()<<"heure: "<<heure;
        qDebug()<<"min: "<<min;

        if(dureeE.getDureeEnMinutes() > t->getScheduledTimeLeft().getDureeEnMinutes())
            throw(CalendarException("Duree Event > Duree tache"));

        if(!(t->isPreemptive()))
            if(dureeE.getDureeEnHeures()>12)
                throw(CalendarException("Event task impossible: duree > 12h"));


        /** Test par rapport à tous les event deja enregistre dans le Manager **/
        for(EventManager::Iterator it=EventManager::getInstance().iterator();!it.isDone(); it.next()) //tant que l'on a pas fait tous les event
        {
            if((dateDebut<it.currentItem().getDateDebut() && dateFin>it.currentItem().getDateDebut())||
               !(dateDebut>=it.currentItem().getDateFin()))
                throw(CalendarException("Incompatibilité date"));
        }

    isOk=true;
    return isOk;
}

/** Add Event **/
EventTraditionnal& EventManager::addEventTrad(const QString& name, const QDateTime &dateDebut, const QDateTime &dateFin, int nbPersonne, QString description){
    EventTraditionnal* e=new EventTraditionnal(name,dateDebut,dateFin,nbPersonne,description);
    this->getInstance().addObject(*e);
    return *e;
}

EventTask& EventManager::addEventTask(const QString& name, const QDateTime &dateDebut, const QDateTime &dateFin,const UnitTask& t){
    EventTask* e=new EventTask(name,dateDebut,dateFin,t);
    this->getInstance().addObject(*e);
    return *e;
}

TIME::Duree calculDuree(const QDateTime &dateDebut, const QDateTime &dateFin){
    TIME::Duree dureeE=0;
    int i1=dateDebut.toTime_t() ;
    int i2=dateFin.toTime_t();
    int heure=(i2-i1)/3600;
    int min=((i2-i1)%3600)/60;
    dureeE.setDuree(heure,min);
    return dureeE;
}

QString Event::getName()const{return _nameE;}



