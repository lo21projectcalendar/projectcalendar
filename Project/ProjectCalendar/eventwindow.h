#ifndef EVENTWINDOW_H
#define EVENTWINDOW_H

#include "projectwindow.h"
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QTextEdit>
#include <QSpinBox>
#include <QTreeView>
#include <QCheckBox>
#include <QScrollArea>
#include "agendawidget.h"
#include "projecteventswidget.h"
#include "projectwindow.h"
#include "calendar.h"

/*!
 * \brief Fenêtre de l'Agenda et des évènements
 */
class EventWindow:public QMainWindow{
Q_OBJECT
public:
    EventWindow();

signals:
    void signalChEvtTrad(const EventTraditionnal&);
    void signalChEvtTask(const EventTask&);
    //void signalModifEvt(const Event&);

public slots:
     void slotInterfaceAddEvtTrad();
     void slotInterfaceAddEvtTask();

     void slotAddEvtTrad();
     void slotAddEvtTask();
     void slotAddProject();
     void slotOpenHardDrive();
     void slotSaveXML();

     void slotInterfaceModifEvtTrad(const EventTraditionnal&);
     void slotInspectorEvtTask(const EventTask&);

     void slotModifEvtTrad();

     void slotInterfaceModifTask(Task&);
     void slotInterfaceModifProject(Project&);

     void slotDateSelected(QDate);
     void slotProjectDoubleClicked(Project&);
     void slotProjectWindowClosed();

     void slotChangeSelectedProject(const QString &);
     void slotEventSelected(Event&);

     void slotProjectSelected(Project&);
     void slotTaskSelected(Task&);

private:

    Task* _task;
    /** Fonction de la class **/

    /** Fenetre Principal (Menu + affichage calendrier)**/
    QMenu* _menuFichier;        //Pour quitter, charger un fichier xml


    QMenu* _menuNew;        //Sous menu ajouter evenement
    QMenu* _menuOpen;
    QMenu* _menuSave;


    QAction* _actionAddEventTrad;
    QAction* _actionAddEventTask;
    QAction* _actionAddProject;

    QAction* _actionLoadHD;
    QAction* _actionSaveXML;
    QAction* _actionSaveText;

    QAction* _actionQuitter;

    QToolBar* _toolBarShorcut;  //Toolbar de racourci

    AgendaWidget* _agenda;    //Zone d'affichage de l'agenda
    QDockWidget* _dockRight;    //Dock apparais si demande de modification ou ajout d'évènement
    QDockWidget* _dockLeft;     //Dock d'affichage des nom des évènements

    ProjectEventsWidget* _eventsWidget;

    /** Fenetre du choix de l'evenement à creer **/
    QLabel* _labelChoixEvent;
    QPushButton* _selectEventTrad;
    QPushButton* _selectEventTask;

    /** Fenetre de création de l'evenement **/
    QWidget* _dockAddEvent;

    QLabel* _labelNomEvt;
    QLineEdit* _NomEvt;

    QLabel* _labelDescriptionEvt;
    QTextEdit* _DescriptionEvt;

    QLabel* _labelNbPersoEvt;
    QSpinBox* _nbPersoEvt;

    QLabel* _labelDateEvt;
    QDateEdit* _dateEvt;
    QLabel* _labelTimeEvt;
    QTimeEdit* _timeEvt;

    QLabel* _labelDatefEvt;
    QDateEdit* _datefEvt;

    QLabel* _labelTimefEvt;
    QTimeEdit* _timefEvt;

    QLabel* _labeldureeEvt;
    QSpinBox* _hDureeEvt;
    QSpinBox* _mDureeEvt;

    QLabel* _labelNomTacheEvt;
    QLineEdit* _nomTacheEvt;

    QComboBox* _listTask;

    QLabel* _labelProjectTask;
    QComboBox* _listProject;

    QPushButton* _saveEvent;
    QPushButton* _cancelEvent;

    QHBoxLayout* coucheH1;
    QHBoxLayout* coucheH2;
    QHBoxLayout* coucheH3;
    QHBoxLayout* coucheH4;
    QHBoxLayout* coucheH5;
    QHBoxLayout* coucheH6;
    QHBoxLayout* coucheH7;

    QVBoxLayout* coucheV1;
    QVBoxLayout* coucheV2;
    QVBoxLayout* coucheV3;

    QVBoxLayout* _dockLayoutAE;

    /** Savaugarde des ancienne valeur avant modif **/
    QDateTime*_oldDateFin;
    QDateTime*_oldDateDebut;
};

#endif // EVENTWINDOW_H

